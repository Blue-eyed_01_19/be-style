﻿using System;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls;
using BeStyle.WebUI.Security;

namespace BeStyle.Admin.WebUI.Pages
{
    public partial class Login : System.Web.UI.Page
    {
        readonly private ISecurityManager _sm = new SecurityManager(ConfigurationManager.ConnectionStrings[@"BeStyleDBConnectionString"].ConnectionString);

        protected void Page_Load(object sender, EventArgs e)
        {
            if (_sm.IsAuthenticated)
            {
                Response.Redirect("~/Pages/Master/Home.aspx");
            }
        }

        protected void lgnLogin_OnLoggingIn(object sender, LoginCancelEventArgs e)
        {
            FormsAuthentication.Initialize();
            if (!IsValidInput())
            {
                return;
            }
            if (!_sm.Login(lgnLogin.UserName, lgnLogin.Password, lgnLogin.RememberMeSet))
            {
                // bad password
            }
        }

        private bool IsValidInput()
        {
            const string userNamePattern = @"[\w]{5,50}";
            const string emailPattern = @"[A-Za-z0-9_-]+@[A-Za-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$";
            const string passwordPattern = @"[\w./()@=\-?\*,]{5,}";
            var regUserName = new Regex(userNamePattern);
            var regEmail = new Regex(emailPattern);
            var regPassword = new Regex(passwordPattern);
            if (!regUserName.IsMatch(lgnLogin.UserName) && !regEmail.IsMatch(lgnLogin.UserName))
            {
                return false;
            }
            if (!regPassword.IsMatch(lgnLogin.Password))
            {
                return false;
            }

            return true;
        }

        protected void lgnLogin_OnLoginError(object sender, EventArgs e)
        {
            lgnLogin.FailureText = "Your login attempt was not successful. Please try again."; ;
        }

      

    }
}