﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LoginMasterPage.Master" AutoEventWireup="true" CodeBehind="Error.aspx.cs" Inherits="BeStyle.Admin.WebUI.Pages.Error" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" runat="server">
    <div class="error">
        <div>
            <asp:Label ID="lblWhat" runat="server" Text="" SkinId="lblError"></asp:Label>
        </div>
        <br />
        <div>
            <asp:Label ID="lblWhy" runat="server" Text="" SkinId="lblError"></asp:Label>
        </div>
        <br />
        <div>
            <asp:Label ID="lblSuggestion" runat="server" Text="" SkinId="lblError"></asp:Label>
        </div>
    </div>
</asp:Content>
