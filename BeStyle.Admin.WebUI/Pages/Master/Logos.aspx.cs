﻿using BeStyle.Entities;
using BeStyle.Repositories.Abstract;
using BeStyle.Repositories.Sql;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BeStyle.Admin.WebUI.Pages.Master
{
    public partial class Logos : System.Web.UI.Page
    {
        [Dependency]
        public ILogoRepository logoRepository { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void obsLogos_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {

            e.ObjectInstance = logoRepository;
        }
        public string GetImage(object img)
        {
            {
                return "data:image/png;base64," + Convert.ToBase64String((byte[])img);
            }
        }

        protected void btnInsert_Click(object sender, EventArgs e)
        {
            LogoEntity logo = new LogoEntity();
            FileUpload flup = (FileUpload)gvLogos.FooterRow.FindControl("flupImage");
            if (flup.HasFile)
            {
                byte[] logoImage = flup.FileBytes;
                logo.Picture = logoImage;
            }            
            logoRepository.CreateLogo(logo);
            gvLogos.DataBind();
        }

    }
}