﻿using BeStyle.Entities;
using BeStyle.Repositories.Abstract;
using BeStyle.Repositories.Sql;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BeStyle.Admin.WebUI.Pages.Master
{
    public partial class ProdCategory : System.Web.UI.Page
    {
        [Dependency]
        public ICategoryRepository categoryRepository { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void obsCategory_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = categoryRepository;
        }

        protected void btnInsert_Click(object sender, EventArgs e)
        {
            CategoryEntity category = new CategoryEntity();
            category.Category = ((TextBox)gvCategory.FooterRow.FindControl("tbxName")).Text;            
            categoryRepository.CreateCategory(category);
            gvCategory.DataBind();
        }
    }
}