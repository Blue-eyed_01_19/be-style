﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using BeStyle.Entities;
using BeStyle.Entities.Enums;
using BeStyle.Repositories.Sql;
using Microsoft.Practices.Unity;
using BeStyle.Repositories.Abstract;


namespace BeStyle.Admin.WebUI.Pages.Master
{
    public partial class Admins : System.Web.UI.Page
    {
        [Dependency]
        public IUserRepository userRepository { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
        }

        protected void btnInsert_OnClick(object sender, EventArgs e)
        {
            var user = new UserEntity();
            user.FirstName = ((TextBox)gvUsers.FooterRow.FindControl("tbxFirstName")).Text;
            user.LastName = ((TextBox)gvUsers.FooterRow.FindControl("tbxLastName")).Text;
            user.Email = ((TextBox)gvUsers.FooterRow.FindControl("tbxEmail")).Text;
            user.Phone = ((TextBox)gvUsers.FooterRow.FindControl("tbxPhone")).Text;
            user.Login = ((TextBox)gvUsers.FooterRow.FindControl("tbxLogin")).Text;
            user.Password = ((TextBox)gvUsers.FooterRow.FindControl("tbxPassword")).Text;
            user.IsDisabled = ((CheckBox)gvUsers.FooterRow.FindControl("cbxDisabled")).Checked;

            if (((CheckBox)gvUsers.FooterRow.FindControl("cbxMaster")).Checked)
            {
                user.Roles.Add(UserRoles.Master.ToString());
            }
            if (((CheckBox)gvUsers.FooterRow.FindControl("cbxModerator")).Checked)
            {
                user.Roles.Add(UserRoles.Moderator.ToString());
            }
            if (((CheckBox)gvUsers.FooterRow.FindControl("cbxEditor")).Checked)
            {
                user.Roles.Add(UserRoles.Editor.ToString());
            }
            if (((CheckBox)gvUsers.FooterRow.FindControl("cbxUser")).Checked)
            {
                user.Roles.Add(UserRoles.User.ToString());
            }
            

            if (!userRepository.CreateUser(user))
            {
               
            }
            else
            {
                gvUsers.DataBind();
            }
        }

        protected void odsUsers_OnObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = userRepository;
        }
       
    }
}