﻿using BeStyle.Repositories.Abstract;
using BeStyle.Repositories.Sql;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using BeStyle.Entities;

namespace BeStyle.Admin.WebUI.Pages.Master
{
    public partial class Orders : System.Web.UI.Page
    {
        [Dependency]
        public IProductRepository productRepository { get; set; }
        [Dependency]
        public ILogoRepository logoRepository { get; set; }
        [Dependency]
        public ISizeRepository sizeRepository { get; set; }
        [Dependency]
        public IOrderRepository orderRepository { get; set; }
        [Dependency]
        public IUserRepository userRepository { get; set; }

        private int _selectedOrderId = 5;
        private int _updateOrderId = -1;
        private bool _isDone = false;


        protected void Page_PreRender()
        {
            try
            {
                if (Session["customer"] != null)
                {
                    string param = (string)Session["customer"];
                    obds_Orders.SelectMethod = "GetAllSubmits";
                    obds_Orders.SelectParameters.Clear();
                    obds_Orders.SelectParameters.Add("cust", param);
                }
                gvOrders.DataBind();
            }
            catch (InvalidOperationException)
            {
                Session["customer"] = "0";
                obds_Orders.SelectMethod = "GetAllSubmits";
                obds_Orders.SelectParameters.Clear();
                obds_Orders.SelectParameters.Add("cust", "0");
                gvOrders.DataBind();
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void obds_Orders_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = orderRepository;
        }

        protected void obds_OrderItems_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = orderRepository;
        }

        protected void obds_OrderItems_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["submitOrderId"] = _selectedOrderId;
        }

        public string GetProductImage(int productId)
        {

            var picture = productRepository.GetProductById(productId).Picture;
            {
                return "data:image/png;base64," + Convert.ToBase64String((byte[])picture);
            }
        }

        public string GetLogoImage(int? logoId)
        {
            if (logoId != null)
            {
                var picture = logoRepository.GetLogoById((int)logoId).Picture;
                {
                    return "data:image/png;base64," + Convert.ToBase64String((byte[])picture);
                }
            }
            else
            {
                return null;
            }
        }

        public string GetSizeValue(int sizeId)
        {
            return sizeRepository.GetSizeValueById(sizeId).Size;
        }

        protected void gvOrders_SelectedIndexChanged(object sender, EventArgs e)
        {
            _selectedOrderId = Convert.ToInt32(((Label)gvOrders.SelectedRow.FindControl("lbl1")).Text);
            gvOrderItems.DataBind();
        }

        protected void gvOrderItems_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private bool SendMail(bool isChecked, int orderId, UserEntity user)
        {
            var fromAddress = new MailAddress("bestyle.team@gmail.com", "BeStyle team");
            var display = string.Format("{0} {1}", user.FirstName, user.LastName);
            var toAddress = new MailAddress(user.Email, display);
            string subject, body;
            var fromPassword = "03061994v";
            if (isChecked)
            {
                subject = "Order complete";
                body = string.Format(
                            "<h2>Dear {0}!</h2><br/>" +
                            "Your order with id = {1} has been successfully completed. Thanks for your order." +
                            "<br/><br/>Best wishes,<br/>" +
                            "The BeStyle Team.",
                            display, orderId);

            }
            else
            {
                subject = "Order cancellation";
                body = string.Format(
                            "<h2>Dear {0}!</h2><br/>" +
                            "Your order with id = {1} has been cancelled. Sorry for the inconvenience." +
                            "<br/><br/>Best wishes,<br/>" +
                            "The BeStyle Team.",
                            display, orderId);
            }

            var sc = new SmtpClient
            {
                Host = @"smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword),
                Timeout = 20000
            };

            using (var msg = new MailMessage()) //fromAddress, toAddress               
            {
                msg.IsBodyHtml = true;
                msg.From = fromAddress;
                msg.To.Add(toAddress);
                msg.Subject = subject;
                msg.Body = body;
                sc.Send(msg);
            }
            sc.Dispose();

            return true;
        }

        protected void obds_Orders_OnUpdating(object sender, ObjectDataSourceMethodEventArgs e)
        {
            e.InputParameters.Clear();
            e.InputParameters["orderId"] = _updateOrderId;
            e.InputParameters["isChecked"] = _isDone;
        }

        protected void gvOrders_OnRowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            _updateOrderId = Convert.ToInt32(((Label) gvOrders.Rows[e.RowIndex].FindControl("lbl1")).Text);
            var cxb = (CheckBox) gvOrders.Rows[e.RowIndex].FindControl("CheckBox1");
            _isDone = cxb.Checked;
            if (cxb.Enabled && _isDone)
            {
                var login = ((Label) gvOrders.Rows[e.RowIndex].FindControl("lbl3")).Text;
                var user = userRepository.GetUserByLogin(login);
                SendMail(_isDone, _updateOrderId, user);
            }
        }

        protected void User_OnLoad(object sender, EventArgs e)
        {
            var ddl = (DropDownList)sender;
            var index = ddl.SelectedIndex;
            ddl.DataSource = userRepository.GetUsersAll();

            ddl.DataValueField = @"Id";
            ddl.DataTextField = @"Login";
            if (!Page.IsPostBack)
            {
                ddl.DataBind();
            }
        }

        protected void CheckBox1_OnPreRender(object sender, EventArgs e)
        {
            var box = sender as CheckBox;
            box.Enabled = !box.Checked;
        }

        protected void ddlUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            var ddl = (DropDownList)sender;
            Session["customer"] = ddl.SelectedValue.ToString(); ;            
        }
    }
}