﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="ProdCategory.aspx.cs" Inherits="BeStyle.Admin.WebUI.Pages.Master.ProdCategory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="center">
        <asp:GridView ID="gvCategory" runat="server" BackColor="White" BorderColor="#CCCCCC"
            BorderStyle="None" ShowFooter="True" BorderWidth="1px" CellPadding="3"
            DataSourceID="obsCategory" AutoGenerateColumns="False" Width="214px" DataKeyNames="Id">
            <Columns>
                <asp:TemplateField HeaderText="ID" SortExpression="ID">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblId" Text='<%#Bind("Id")%>' />
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Button ID="btnInsert" runat="server" Text="Add" OnClick="btnInsert_Click" ValidationGroup="ProductPageValidation"/>
                    </FooterTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Category">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblCategory" Text='<%#Bind("Category") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="tbxCategory" runat="server" Text='<%# Bind("Category") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:TextBox ID="tbxName" runat="server" Text='<%#Bind("Category") %>'></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqValNameEdit" runat="server" ErrorMessage="Name can't be empty"
                            ControlToValidate="tbxName" ValidationGroup="ProductPageValidation" Display="None"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="regValNameEdit" runat="server" Display="None" ErrorMessage="Name is invalid"
                            ValidationGroup="ProductPageValidation" ValidationExpression="[A-Za-zА-Яа-яІЇёЁЄє-]{2,}"
                            ControlToValidate="tbxName" />
                    </FooterTemplate>
                </asp:TemplateField>

                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton ID="btnDelete" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>

            </Columns>

            <FooterStyle BackColor="White" ForeColor="#000066" />
            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            <RowStyle ForeColor="#000066" />
            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#007DBB" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#00547E" />
        </asp:GridView>
    </div>

    <asp:ObjectDataSource ID="obsCategory" runat="server" SelectMethod="GetAllCategory" OnObjectCreating="obsCategory_ObjectCreating"
        TypeName="BeStyle.Repositories.Sql.CategoryRepository" DeleteMethod="DeleteCategory">
        <DeleteParameters>
            <asp:Parameter Name="Id" Type="Int32" />
        </DeleteParameters>
    </asp:ObjectDataSource>

</asp:Content>
