﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" Theme="DefaultTheme" AutoEventWireup="true" CodeBehind="Admins.aspx.cs" Inherits="BeStyle.Admin.WebUI.Pages.Master.Admins" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">

    <div class="center">
        <div>
            <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></cc1:ToolkitScriptManager>
            <asp:GridView ID="gvUsers" runat="server" DataKeyNames="Id" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None"
                BorderWidth="1px" CellPadding="3" DataSourceID="odsUsers" ShowFooter="True" AutoGenerateColumns="False" AllowPaging="True" PageSize="10">
                <Columns>
                    <asp:TemplateField HeaderText="Id" SortExpression="Id">
                        <EditItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("Id") %>'></asp:Label>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("Id") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Button runat="server" ID="btnInsert" Text="Add" ValidationGroup="AdminPageValidation" OnClick="btnInsert_OnClick" />
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="FirstName" SortExpression="FirstName" ValidateRequestMode="Enabled">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("FirstName") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqValFirstNameEdit" runat="server" ErrorMessage="First name can't be empty"
                                ControlToValidate="TextBox1" ValidationGroup="AdminPageValidationEdit" Display="None"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="regValFirstNameEdit" runat="server" Display="None" ErrorMessage="First name is invalid"
                                ValidationGroup="AdminPageValidationEdit" ValidationExpression="[A-Za-zА-Яа-яІЇёЁЄє-]{2,}"
                                ControlToValidate="TextBox1" />

                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("FirstName") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox runat="server" ID="tbxFirstName" Text='<%# Bind("FirstName") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqValFirstName" runat="server" ErrorMessage="First name can't be empty"
                                ControlToValidate="tbxFirstName" ValidationGroup="AdminPageValidation" Display="None"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="regValFirstName" runat="server" Display="None" ErrorMessage="First name is invalid"
                                ValidationGroup="AdminPageValidation" ValidationExpression="[A-Za-zА-Яа-яІЇёЁЄє-]{2,}"
                                ControlToValidate="tbxFirstName" />
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="LastName" SortExpression="LastName">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("LastName") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqValLastNameEdit" runat="server" ErrorMessage="Last name can't be empty"
                                ControlToValidate="TextBox2" ValidationGroup="AdminPageValidationEdit" Display="None"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="regValLastNameEdit" runat="server" Display="None" ErrorMessage="First name is invalid"
                                ValidationGroup="AdminPageValidationEdit" ValidationExpression="[A-Za-zА-Яа-яІЇёЁЄє-]{2,}"
                                ControlToValidate="TextBox2" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("LastName") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox runat="server" ID="tbxLastName" Text='<%# Bind("LastName") %>'> </asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqValLastName" runat="server" ErrorMessage="Last name can't be empty"
                                ControlToValidate="tbxLastName" ValidationGroup="AdminPageValidation" Display="None"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="regValLastName" runat="server" Display="None" ErrorMessage="First name is invalid"
                                ValidationGroup="AdminPageValidation" ValidationExpression="[A-Za-zА-Яа-яІЇёЁЄє-]{2,}"
                                ControlToValidate="tbxLastName" />
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Email" SortExpression="Email">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Email") %>'>
                            </asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqValEmailEdit" runat="server" ErrorMessage="Email can't be empty"
                                ControlToValidate="TextBox3" ValidationGroup="AdminPageValidationEdit" Display="None"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="regValEmailEdit" runat="server" Display="None" ErrorMessage="Email is invalid"
                                ValidationGroup="AdminPageValidationEdit" ValidationExpression="[A-Za-z0-9_-]+@[A-Za-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$"
                                ControlToValidate="TextBox3" />

                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("Email") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox runat="server" ID="tbxEmail" Text='<%# Bind("Email") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqValEmail" runat="server" ErrorMessage="Email can't be empty"
                                ControlToValidate="tbxEmail" ValidationGroup="AdminPageValidation" Display="None"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="regValEmail" runat="server" Display="None" ErrorMessage="Email is invalid"
                                ValidationGroup="AdminPageValidation" ValidationExpression="[A-Za-z0-9_-]+@[A-Za-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$"
                                ControlToValidate="tbxEmail" />
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Phone" SortExpression="Phone">
                        <EditItemTemplate>

                            <asp:TextBox ID="tbxPhone" runat="server" Text='<%# Bind("Phone") %>'></asp:TextBox>
                            <cc1:MaskedEditExtender ID="TextBox_MaskedEditExtender" runat="server" Enabled="True" Mask="(999) 999 9999" MaskType="Number" TargetControlID="tbxPhone">
                            </cc1:MaskedEditExtender>
                            <asp:RegularExpressionValidator ID="regValPhoneEdit" runat="server" Display="None" ErrorMessage="Phone is invalid. Use next format: (xxx) xxx xxxx"
                                ValidationGroup="AdminPageValidationEdit" ValidationExpression="^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$"
                                ControlToValidate="tbxPhone" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("Phone") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox runat="server" ID="tbxPhone" Text='<%# Bind("Phone") %>'></asp:TextBox>
                            <cc1:MaskedEditExtender ID="TextBox_MaskedEditExtender" runat="server" Enabled="True" Mask="(999) 999 9999" MaskType="Number" TargetControlID="tbxPhone">
                            </cc1:MaskedEditExtender>
                            <asp:RegularExpressionValidator ID="regValPhone" runat="server" Display="None" ErrorMessage="Phone is invalid. Use next format: (xxx) xxx xxxx"
                                ValidationGroup="AdminPageValidation" ValidationExpression="^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$"
                                ControlToValidate="tbxPhone" />
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Login" SortExpression="Login">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("Login") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqValLoginEdit" runat="server" ErrorMessage="Login can't be empty"
                                ControlToValidate="TextBox5" ValidationGroup="AdminPageValidation" Display="None"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="regValLoginEdit" runat="server" Display="None" ErrorMessage="Login is invalid"
                                ValidationGroup="AdminPageValidationEdit" ValidationExpression="[\w]{5,50}"
                                ControlToValidate="TextBox5" />

                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label6" runat="server" Text='<%# Bind("Login") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox runat="server" ID="tbxLogin" Text='<%# Bind("Login") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqValLogin" runat="server" ErrorMessage="Login can't be empty"
                                ControlToValidate="tbxLogin" ValidationGroup="AdminPageValidation" Display="None"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="regValLogin" runat="server" Display="None" ErrorMessage="Login is invalid"
                                ValidationGroup="AdminPageValidation" ValidationExpression="[\w]{5,50}"
                                ControlToValidate="tbxLogin" />
                        </FooterTemplate>
                    </asp:TemplateField>                    
                    <asp:TemplateField HeaderText="IsMaster" SortExpression="IsMaster">
                        <EditItemTemplate>
                            <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("IsMaster") %>' />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("IsMaster") %>' Enabled="false" />
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox runat="server" ID="cbxMaster" Checked='<%# Bind("IsMaster") %>' />
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="IsModerator" SortExpression="IsModerator">
                        <EditItemTemplate>
                            <asp:CheckBox ID="CheckBox2" runat="server" Checked='<%# Bind("IsModerator") %>' />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBox2" runat="server" Checked='<%# Bind("IsModerator") %>' Enabled="false" />
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox runat="server" ID="cbxModerator" Checked='<%# Bind("IsModerator") %>' />
                        </FooterTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="IsEditor" SortExpression="IsEditor">
                        <EditItemTemplate>
                            <asp:CheckBox ID="CheckBox3" runat="server" Checked='<%# Bind("IsEditor") %>' />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBox3" runat="server" Checked='<%# Bind("IsEditor") %>' Enabled="false" />
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox runat="server" ID="cbxEditor" Checked='<%# Bind("IsEditor") %>' />
                        </FooterTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="IsUser" SortExpression="IsUser">
                        <EditItemTemplate>
                            <asp:CheckBox ID="CheckBox4" runat="server" Checked='<%# Bind("IsUser") %>' />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBox4" runat="server" Checked='<%# Bind("IsUser") %>' Enabled="false" />
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox runat="server" ID="cbxUser" Checked='<%# Bind("IsUser") %>' />
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="IsDisabled" SortExpression="IsDisabled">
                        <EditItemTemplate>
                            <asp:CheckBox ID="CheckBox5" runat="server" Checked='<%# Bind("IsDisabled") %>' />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBox5" runat="server" Checked='<%# Bind("IsDisabled") %>' Enabled="false" />
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox runat="server" ID="cbxDisabled" Checked='<%# Bind("IsDisabled") %>' />
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <EditItemTemplate>
                            <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="True" ValidationGroup="AdminPageValidationEdit" CommandName="Update" Text="Update"></asp:LinkButton>
                            &nbsp;<asp:LinkButton ID="LinkButton3" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton4" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="White" ForeColor="#000066" />
                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                <RowStyle ForeColor="#000066" />
                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                <SortedAscendingHeaderStyle BackColor="#007DBB" />
                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                <SortedDescendingHeaderStyle BackColor="#00547E" />
            </asp:GridView>
            <asp:ObjectDataSource ID="odsUsers" runat="server" DeleteMethod="DeleteUser" UpdateMethod="UpdateUserInfo" SelectMethod="GetUsersAll" OnObjectCreating="odsUsers_OnObjectCreating" TypeName="BeStyle.Repositories.Sql.UserRepository"></asp:ObjectDataSource>
            <br />
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowSummary="True" ForeColor="Red" ValidationGroup="AdminPageValidation" />
            <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowSummary="True" ForeColor="Red" ValidationGroup="AdminPageValidation" />
        </div>
    </div>
</asp:Content>
