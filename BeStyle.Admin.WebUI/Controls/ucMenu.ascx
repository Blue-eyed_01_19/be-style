﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucMenu.ascx.cs" Inherits="BeStyle.Admin.WebUI.Controls.ucMenu" %>
<div style="font-family: Arial">
    <asp:LoginView ID="lv1" runat="server">
        <LoggedInTemplate>
            Wellcome
            <asp:LoginName runat="server" ID="ln1" ForeColor="#ff3300" Font-Bold="true" Font-Size="13" /> |
                <%--<asp:HyperLink ID="linkHome" runat="server" Text="Visit Our site" NavigateUrl="#" /> --%>
        </LoggedInTemplate>
    </asp:LoginView>
    <asp:LoginStatus runat="server" ID="ls1" Font-Names="Arial" />
</div>
