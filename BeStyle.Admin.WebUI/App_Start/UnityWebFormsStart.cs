using System.Web;
using Microsoft.Practices.Unity;
using Unity.WebForms;
using BeStyle.Repositories.Abstract;
using BeStyle.Repositories.Sql;
using BeStyle.WebUI.Security;
using NLog;
using System.Web.Configuration;

[assembly: WebActivator.PostApplicationStartMethod(typeof(BeStyle.Admin.WebUI.App_Start.UnityWebFormsStart), "PostStart")]
namespace BeStyle.Admin.WebUI.App_Start
{
    
    internal static class UnityWebFormsStart
    {     
        internal static void PostStart()
        {
            IUnityContainer container = new UnityContainer();
            HttpContext.Current.Application.SetContainer(container);
            RegisterDependencies(container);
        }    
        private static void RegisterDependencies(IUnityContainer container)
        {
            string connectionString = WebConfigurationManager.ConnectionStrings["BeStyleDBConnectionString"].ConnectionString;
            container.RegisterType<IUserRepository, UserRepository>(new InjectionConstructor(connectionString));
            container.RegisterType<ILogoRepository, LogoRepository>(new InjectionConstructor(connectionString));
            container.RegisterType<ICategoryRepository, CategoryRepository>(new InjectionConstructor(connectionString));
            container.RegisterType<IProductRepository, ProductRepository>(new InjectionConstructor(connectionString));
            container.RegisterType<ISecurityManager, SecurityManager>(new InjectionConstructor(connectionString));
            container.RegisterType<ISizeRepository, SizeRepository>(new InjectionConstructor(connectionString));
            container.RegisterType<IOrderRepository, OrderRepository>(new InjectionConstructor(connectionString));
            container.RegisterType<ILogger, Logger>(new InjectionFactory(f => LogManager.GetCurrentClassLogger()));
        }
    }   
}