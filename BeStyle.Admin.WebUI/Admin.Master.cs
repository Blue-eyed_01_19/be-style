﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;
using System.Web.Security;
using BeStyle.Entities.Enums;

namespace BeStyle.Admin.WebUI
{
    public partial class Admin : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //throw new ArgumentException("It's a test error!!!");
        }

        protected void menuAdmin_MenuItemDataBound(object sender, MenuEventArgs e)
        {            
            System.Web.UI.WebControls.Menu menu = (System.Web.UI.WebControls.Menu)sender;
            SiteMapNode mapNode = (SiteMapNode)e.Item.DataItem;

            if (HttpContext.Current.User.IsInRole(UserRoles.Master.ToString()))
            {

            }
            #region Hiding MenuItems for "Moderator"
            else if (this.Page.User.IsInRole(UserRoles.Moderator.ToString()))
            {
                if (mapNode.Title == "Admins")
                {
                    System.Web.UI.WebControls.MenuItem parent = e.Item.Parent;
                    if (parent != null)
                    {
                        parent.ChildItems.Remove(e.Item);
                    }
                }
            }
            #endregion

            #region Hiding MenuItems for "Editor"
            else if (this.Page.User.IsInRole(UserRoles.Editor.ToString()))
            {
                if (mapNode.Title == "Admins")
                {
                    System.Web.UI.WebControls.MenuItem parent = e.Item.Parent;
                    if (parent != null)
                    {
                        parent.ChildItems.Remove(e.Item);
                    }
                }
                if (mapNode.Title == "Products")
                {
                    System.Web.UI.WebControls.MenuItem parent = e.Item.Parent;
                    if (parent != null)
                    {
                        parent.ChildItems.Remove(e.Item);
                    }
                }
                if (mapNode.Title == "Logos")
                {
                    System.Web.UI.WebControls.MenuItem parent = e.Item.Parent;
                    if (parent != null)
                    {
                        parent.ChildItems.Remove(e.Item);
                    }
                }
            }
            #endregion

        }
    }
}