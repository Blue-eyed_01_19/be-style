﻿using System;
using System.Web;
using System.Web.Security;
using BeStyle.Repositories.Abstract;
using BeStyle.Repositories.Sql;
using System.Configuration;
using System.Security.Permissions;
using System.Text;
using System.Text.RegularExpressions;
using BeStyle.Entities;

namespace BeStyle.WebUI.Security
{
    public class SecurityManager : ISecurityManager
    {
        #region Private Fields

        private readonly IUserRepository _userRepository;
        private UserInfo _user;
        private UserEntity _userInfo;

        #endregion

        #region Constructors

        public SecurityManager(string connectionString)
        {
            this._userRepository = new UserRepository(connectionString);
            _user = new UserInfo();
            if (IsAuthenticated)
            {
                var user = _userRepository.GetUserByLogin(User.UserName);
                if (user == null)
                {
                    user = _userRepository.GetUserByEmail(User.UserName);
                    if (user != null)
                    {
                        InitUser(user);
                    }
                }
                else
                {
                    InitUser(user);
                }
            }
        }

        #endregion

        #region ISecurityManager

        public bool Login(string userName, string password, bool rememberPass)
        {
            var user = _userRepository.GetUserByLogin(userName);
            if (user == null)
            {
                user = _userRepository.GetUserByEmail(userName);
                if (user == null)
                {                    
                    return false;
                }
            }

            if (user.IsDisabled)
            {
                return false;
            }

            if ((CryptPassword(password) == user.Password) || (password == user.Password))
            {
                InitUser(user); 
                FormsAuthenticationUtil.RedirectFromLoginPage(user.Login, user.CommaSeparatedRoles, rememberPass);
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Logout()
        {
            if (IsAuthenticated)
            {
                _user = new UserInfo();
                FormsAuthentication.SignOut();
            }
        }

        public void UpdateLogin(string newLogin)
        {
            var userName = newLogin;

            HttpCookie cookie = FormsAuthentication.GetAuthCookie(User.UserName, true);
            var ticket = FormsAuthentication.Decrypt(cookie.Value);

            // Store UserData inside the Forms Ticket with all the attributes
            // in sync with the web.config
            var newticket = new FormsAuthenticationTicket(ticket.Version,
                                                          userName,
                                                          ticket.IssueDate,
                                                          ticket.Expiration,
                                                          true, // always persistent
                                                          _userInfo.CommaSeparatedRoles,
                                                          ticket.CookiePath);

            // Encrypt the ticket and store it in the cookie
            cookie.Value = FormsAuthentication.Encrypt(newticket);
            cookie.Expires = newticket.Expiration.AddHours(24);
            HttpContext.Current.Response.Cookies.Set(cookie);
        }

        public string CryptPassword(string password)
        {
            System.Security.Cryptography.MD5CryptoServiceProvider x = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] data = System.Text.Encoding.ASCII.GetBytes(password);
            data = x.ComputeHash(data);
            return System.Text.Encoding.ASCII.GetString(data);
        }

        public bool IsInRole(string role)
        {
            return HttpContext.Current.User.IsInRole(role);
        }

        public bool IsAuthenticated
        {
            get
            {
                return (HttpContext.Current.User != null) && (HttpContext.Current.User.Identity != null) &&
                    HttpContext.Current.User.Identity.IsAuthenticated;
            }
        }

        public UserInfo User
        {
            get
            {
                _user.UserName = (HttpContext.Current.User == null || HttpContext.Current.User.Identity == null) ? ""
                    : HttpContext.Current.User.Identity.Name;
                //if (_user.Login == null)
                //{
                //    _user = new UserInfo(); ;
                //    if (IsAuthenticated)
                //    {
                //        var user = _userRepository.GetUserByLogin(User.UserName);
                //        if (user == null)
                //        {
                //            user = _userRepository.GetUserByEmail(User.UserName);
                //            if (user != null)
                //            {
                //                InitUser(user);
                //            }
                //        }
                //        else
                //        {
                //            InitUser(user);
                //        }
                //    }

                //}
                return _user;
            }
            private set { _user = value; }
        }

        #endregion

        #region Static Methods

        public static void AttachRolesToUser()
        {
            FormsAuthenticationUtil.AttachRolesToUser();
        }

        #endregion

        #region Helpers

        private void InitUser(UserEntity user)
        {
            _user.Login = user.Login;
            _user.FirstName = user.FirstName;
            _user.UserId = user.Id;
            _userInfo = user;
        }

        #endregion
    }
}