﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeStyle.WebUI.Security
{
    public class UserInfo
    {
        public string UserName { get; set; }
        public int UserId { get; set; }
        public string Login { get; set; }
        public string FirstName { get; set; }
    }
}
