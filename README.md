![BeStyle grey.png](https://bitbucket.org/repo/g4Kqen/images/2253228561-BeStyle%20grey.png) 

Idea of our project is internet shop of selling clothes and with possibility of designing your own products. This site will be useful to companies which sell clothes and which provide printing services on textile.
Site will be comfortable for people which appreciate originality. Restaurants, hotels, fanclubs or sport teams will be able to create a form for itself with its own logo.

## Links to the hosted applications ##
Part | Site address | Role | Login | Password
-----|--------------|------|-------|---------
Frontend | http://vladukua-001-site1.myasp.net/ | user | Andriy | Andriy
Admin page | [http://tsapandriy-001-site1.smarterasp.net/Pages/Login.aspx](http://tsapandriy-001-site1.smarterasp.net/Pages/Login.aspx?ReturnUrl=%2fPages%2fMaster%2fOrders.aspx) | master | Vitaly | Vitaly

## The content of the documentation:
* [Project team and roles](https://bitbucket.org/voytyshyn/be-style/wiki/Project%20team%20and%20roles)                                                   
* [UML Component diagram](https://bitbucket.org/voytyshyn/be-style/wiki/UML%20Component%20diagram)
* [UML Deployment diagram](https://bitbucket.org/voytyshyn/be-style/wiki/UML%20Deployment%20diagram)
* [UML Use case diagram](https://bitbucket.org/voytyshyn/be-style/wiki/UML%20Use%20case%20diagram)
* [Unit Test Code Coverage](https://bitbucket.org/voytyshyn/be-style/wiki/Unit%20Tests%20Code%20Coverage)
* [Data Base diagram](https://bitbucket.org/voytyshyn/be-style/wiki/Data%20Base%20diagram)
* [List of the features that should be implemented in the future](https://bitbucket.org/voytyshyn/be-style/wiki/List%20of%20the%20features%20that%20should%20be%20implemented%20in%20the%20future)
* [List of bugs that should be resolved in the next release.](https://bitbucket.org/voytyshyn/be-style/wiki/List%20of%20bugs%20that%20should%20be%20fixed%20in%20the%20next%20release)