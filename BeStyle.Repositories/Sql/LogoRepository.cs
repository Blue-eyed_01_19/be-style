﻿using BeStyle.Entities;
using BeStyle.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeStyle.Repositories.Sql
{
    public class LogoRepository : ILogoRepository
    {
        #region Private Fields

        private readonly string _connectionString;

        #endregion

        #region Queryies

        private const string GET_ALL = "SELECT [Id],[Picture],[IsUser] FROM tblLogo";
        private const string DELETE_BY_ID_QUERY = "DELETE FROM [tblLogo] WHERE [Id]=@LogoId";
        private const string INSERT_QUERY = "INSERT INTO [tblLogo] ([Picture], [IsUser]) Values(@Picture, @IsUser)";
        private const string GET_LOGO_BY_ID = "SELECT [Id],[Picture],[IsUser] FROM [tblLogo] WHERE [Id]=@Id";
        private const string GET_LAST_INSERTED_INDEX = "SELECT IDENT_CURRENT('tblLogo')";
        private const string GET_LOGO_FOR_CURRENT_USER = @"SELECT tblLogo.Picture, tblLogo.Id, tblLogo.IsUser
                                                                FROM tblLogo
                                                                LEFT JOIN tblOrderItem
                                                                ON tblLogo.Id  = tblOrderItem.LogoBackId OR tblLogo.Id  = tblOrderItem.LogoFrontId
                                                                LEFT JOIN tblOrder
                                                                ON tblOrderItem.OrderId = tblOrder.Id
                                                                WHERE tblOrder.UserId = @UserId AND tblLogo.IsUser  = 1
                                                                GROUP BY tblLogo.Picture, tblLogo.Id, tblLogo.IsUser
                                                                HAVING COUNT(tblLogo.Picture) = 1";

        #endregion

        #region Constructors

        public LogoRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        #endregion

        #region ILogoRepository

        public List<LogoEntity> GetAllLogos()
        {
            List<LogoEntity> logos = new List<LogoEntity>();
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(GET_ALL, connection))
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        LogoEntity logo = ReadLogo(reader);
                        logos.Add(logo);
                    }
                    return logos;
                }
            }
        }

        public LogoEntity GetLogoById(int logoId)
        {
            LogoEntity logo = new LogoEntity();
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(GET_LOGO_BY_ID, connection))
                {
                    cmd.Parameters.AddWithValue("@Id", logoId);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        reader.Read();
                        logo = ReadLogo(reader);
                    }
                }
            }
            return logo;
        }

        public void DeleteById(int id)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (var cmd = new SqlCommand(DELETE_BY_ID_QUERY, connection))
                {
                    cmd.Parameters.AddWithValue("LogoId", id);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void CreateLogo(LogoEntity logo)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(INSERT_QUERY, connection))
                {
                    cmd.Parameters.Add("@Picture", SqlDbType.VarBinary).Value = logo.Picture;
                    cmd.Parameters.Add("@IsUser", SqlDbType.Bit).Value = 1;
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public int GetLastInsertedId()
        {
            int id;
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(GET_LAST_INSERTED_INDEX, connection))
                {
                    id = (int)(decimal)cmd.ExecuteScalar();
                }
            }
            return id;
        }

        public List<LogoEntity> GetLogosForCurrentUser(int UserId)
        {
            List<LogoEntity> logos = new List<LogoEntity>();
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(GET_LOGO_FOR_CURRENT_USER, connection))
                {
                    cmd.Parameters.AddWithValue("@UserId", UserId);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        LogoEntity logo = ReadLogo(reader);
                        logos.Add(logo);
                    }
                }
            }
            return logos;
        }

        #endregion

        #region Helpers

        private LogoEntity ReadLogo(SqlDataReader reader)
        {
            LogoEntity logo = new LogoEntity();
            logo.Id = (int)reader["Id"];
            logo.Picture = (byte[])reader["Picture"];
            logo.IsUser = (bool)reader["IsUser"];
            return logo;
        }

        #endregion
    }
}
