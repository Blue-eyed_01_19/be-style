﻿using BeStyle.Repositories.Abstract;
using BeStyle.Entities;
using System.Collections.Generic;
using System.Data.SqlClient;
using System;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using BeStyle.Repositories.Enums;


namespace BeStyle.Repositories.Sql
{
    public class UserRepository : IUserRepository
    {
        #region Queryies

        private const string GET_ALL_QUERY = "SELECT * FROM tblUser";
        private const string GET_ALL_ROLES_FOR_USER = "SELECT [Id],[Role] FROM tblUserRole WHERE UserId=@Id";
        private const string UPDATE_USER = @"UPDATE tblUser SET FirstName=@FirstName, LastName=@LastName, Email=@Email, Phone=@Phone, Login=@Login, Password=@Password, Disabled=@Disabled WHERE Id=@Id";
        private const string UPDATE_MAIN_USER_INFO = @"UPDATE tblUser SET FirstName=@FirstName, LastName=@LastName, Email=@Email, Phone=@Phone, Login=@Login WHERE Id=@Id";
        private const string IS_USER = "SELECT * FROM tblUser WHERE Login=@Login";
        private const string GET_USER_BY_EMAIL = @"SELECT * FROM tblUser WHERE Email=@Email";
        private const string GET_USER_BY_ID = @"SELECT * FROM tblUser WHERE Id=@Id";
        private const string GET_ALL_ROLE_VALUES_FOR_USER_ID = @"SELECT role.Role
                                               FROM tblUserRole roleid
                                               INNER JOIN tblRole role
                                               ON roleid.RoleId=role.id 
                                               WHERE roleid.UserId=@Id";
        private const string CHANGE_PASSWORD_FOR_USER = @"UPDATE tblUser SET Password=@Password WHERE Id=@Id";
        private const string ACTIVATE_USER =
            @"UPDATE tblUser SET Disabled=@Disabled, ActivationCode=@ActivationCode WHERE Id=@Id";
        private const string RESTORE_PASSWORD = @"UPDATE tblUser SET Password=@Password WHERE Id=@Id";

        #endregion

        #region Private Fields

        private readonly string _connectionString;
        private string _commandString;

        #endregion

        #region Constructors

        public UserRepository(string connectionString)
        {
            this._connectionString = connectionString;
        }

        #endregion

        #region IUserRepository

        public void ActivateUser(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (var cmd = new SqlCommand(ACTIVATE_USER, connection))
                {
                    cmd.Parameters.AddWithValue("Id", id);
                    cmd.Parameters.AddWithValue("ActivationCode", Guid.Empty);
                    cmd.Parameters.AddWithValue("Disabled", false);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public List<UserEntity> GetUsersAll()
        {
            List<UserEntity> users = new List<UserEntity>();
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(GET_ALL_QUERY, connection))
                {
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            UserEntity user = ReadUser(reader);
                            users.Add(user);
                        }
                        return users;
                    }
                }
            }
        }

        public void UpdateUserInfo(int Id, string FirstName, string LastName, string Email, string Phone, string Login,
                            string Password, bool IsMaster, bool IsModerator, bool IsEditor, bool IsUser, bool IsDisabled)
        {
            var user = new UserEntity()
            {
                Id = Id,
                FirstName = FirstName ?? string.Empty,
                LastName = LastName ?? string.Empty,
                Email = Email,
                Phone = Phone ?? string.Empty,
                Login = Login,
                Password = Password,
                IsDisabled = IsDisabled
            };
            if (IsMaster)
            {
                user.Roles.Add("Master");
            }
            if (IsModerator)
            {
                user.Roles.Add("Moderator");
            }
            if (IsEditor)
            {
                user.Roles.Add("Editor");
            }
            if (IsUser)
            {
                user.Roles.Add("User");
            }
            UpdateUserInfo(user);
        }

        public List<string> GetAllRolesForUser(int userId)
        {
            var roles = new List<string>();
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(GET_ALL_ROLES_FOR_USER, connection))
                {
                    command.Parameters.AddWithValue("Id", userId);
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string role = ReadRole(reader);
                            roles.Add(role);
                        }
                        return roles.Count == 0 ? null : roles;
                    }
                }
            }
        }

        public List<string> GetAllRolesValueForUser(int userId)
        {
            List<string> roles = new List<string>();
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(GET_ALL_ROLE_VALUES_FOR_USER_ID, connection))
                {
                    cmd.Parameters.AddWithValue("Id", userId);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string role = ReadRole(reader);
                            roles.Add(role);
                        }
                    }
                }
            }
            return roles.Count == 0 ? null : roles;

        }

        public void UpdateUserInfo(UserEntity user)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (var cmd = new SqlCommand(UPDATE_USER, connection))
                {
                    cmd.Parameters.AddWithValue("FirstName", user.FirstName);
                    cmd.Parameters.AddWithValue("LastName", user.LastName);
                    cmd.Parameters.AddWithValue("Email", user.Email);
                    cmd.Parameters.AddWithValue("Phone", user.Phone);
                    cmd.Parameters.AddWithValue("Login", user.Login);
                    cmd.Parameters.AddWithValue("Password", user.Password);
                    cmd.Parameters.AddWithValue("Id", user.Id);
                    cmd.Parameters.AddWithValue("Disabled", user.IsDisabled);
                    cmd.ExecuteNonQuery();
                    UpdateRoles(user.Id, SetRoleId(user.Roles));
                }
            }
        }

        public UpdateUserInfo UpdateMainUserInfo(UserEntity user)
        {
            var oldUser = GetUserById(user.Id);
            var userByLogin = GetUserByLogin(user.Login);
            var userByEmail = GetUserByEmail(user.Email);
            if (oldUser.Login != user.Login && oldUser.Email != user.Email)
            {
                if (userByLogin != null && userByEmail != null)
                {
                    return Enums.UpdateUserInfo.LoginAndEmailIsBusy; ;
                }
            }
            else if (oldUser.Login != user.Login)
            {
                if (userByLogin != null)
                {
                    return Enums.UpdateUserInfo.LoginIsBusy;
                }
            }
            else if (oldUser.Email != user.Email)
            {
                if (userByEmail != null)
                {
                    return Enums.UpdateUserInfo.EmailIsBusy;
                }
            }

            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (var cmd = new SqlCommand(UPDATE_MAIN_USER_INFO, connection))
                {
                    cmd.Parameters.AddWithValue("FirstName", user.FirstName);
                    cmd.Parameters.AddWithValue("LastName", user.LastName);
                    cmd.Parameters.AddWithValue("Email", user.Email);
                    cmd.Parameters.AddWithValue("Phone", user.Phone ?? "");
                    cmd.Parameters.AddWithValue("Login", user.Login);
                    cmd.Parameters.AddWithValue("Id", user.Id);
                    cmd.ExecuteNonQuery();
                }
            }
            return Enums.UpdateUserInfo.Success;
        }

        public void ChangePassword(int id, string password)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (var cmd = new SqlCommand(CHANGE_PASSWORD_FOR_USER, connection))
                {
                    cmd.Parameters.AddWithValue("Id", id);
                    cmd.Parameters.AddWithValue("Password", password);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void DeleteUser(int userId)
        {
            DeleteRolesForUser(userId);
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (var cmd = new SqlCommand(UPDATE_USER, connection))
                {
                    cmd.Parameters.AddWithValue("UserId", userId);
                    cmd.ExecuteNonQuery();
                }
            }

        }

        public bool CreateUser(UserEntity user)
        {
            user.Password = CreateHash(user.Password);
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (var cmd = new SqlCommand(IS_USER, connection))
                {
                    cmd.Parameters.AddWithValue("Login", user.Login);
                    var ret = cmd.ExecuteScalar();
                    if (ret != null)
                    {
                        return false;
                    }
                    _commandString = string.Format("SELECT Id FROM tblUser WHERE Email='{0}'", user.Email);
                    cmd.CommandText = _commandString;
                    ret = cmd.ExecuteScalar();
                    if (ret != null)
                    {
                        return false; ;
                    }
                }
                var query = string.Format("INSERT INTO tblUser (FirstName, LastName, Email, Phone, Login, Password, Disabled, ActivationCode) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', @Disabled, @ActivationCode);\n\rSELECT SCOPE_IDENTITY();",
                                            user.FirstName, user.LastName, user.Email,
                                            user.Phone, user.Login, user.Password);
                using (var cmd = new SqlCommand(query, connection))
                {
                    cmd.Parameters.AddWithValue("Disabled", user.IsDisabled);
                    cmd.Parameters.AddWithValue("ActivationCode", user.ActivationCode);
                    var id = (int)(decimal)cmd.ExecuteScalar();
                    CreateRoles(id, SetRoleId(user.Roles));
                }
            }
            return true;
        }

        public UserEntity GetUserByLogin(string login)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                _commandString = String.Format(@"SELECT * FROM tblUser WHERE Login='{0}'", login);
                using (SqlCommand cmd = new SqlCommand(_commandString, connection))
                {
                    using (SqlDataReader reader = cmd.ExecuteReader())
                        if (reader.Read())
                        {
                            return ReadUser(reader);
                        }
                        else
                        {
                            return null;
                        }
                }
            }
        }

        public UserEntity GetUserByEmail(string email)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (var cmd = new SqlCommand(GET_USER_BY_EMAIL, connection))
                {
                    cmd.Parameters.AddWithValue("Email", email);
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return ReadUser(reader);
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
        }

        public UserEntity GetUserById(int Id)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (var cmd = new SqlCommand(GET_USER_BY_ID, connection))
                {
                    cmd.Parameters.AddWithValue("Id", Id);
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return ReadUser(reader);
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
        }

        public void RestorePassword(int id, string password)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (var cmd = new SqlCommand(RESTORE_PASSWORD, connection))
                {
                    var rnd = new Random(DateTime.Now.Millisecond);
                    cmd.Parameters.AddWithValue("Id", id);
                    cmd.Parameters.AddWithValue("Password", password);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        #endregion

        #region Helpers

        private void DeleteRolesForUser(int userId)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                _commandString = @"DELETE FROM tblUserRole WHERE UserId=@UserId";
                using (var cmd = new SqlCommand(_commandString, connection))
                {
                    cmd.Parameters.AddWithValue("@UserId", userId);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        private void UpdateRoles(int userId, IEnumerable<int> roles)
        {
            DeleteRolesForUser(userId);
            CreateRoles(userId, roles);
        }

        private void CreateRoles(int userId, IEnumerable<int> roles)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                var insertQuery = new StringBuilder();
                foreach (var roleId in roles)
                {
                    insertQuery.AppendLine(string.Format("INSERT INTO tblUserRole (UserId, RoleId) VALUES ({0}, {1});", userId, roleId));
                }
                using (var cmd = new SqlCommand(insertQuery.ToString(), connection))
                {
                    cmd.ExecuteNonQuery();
                }
            }

        }

        private UserEntity ReadUser(SqlDataReader reader)
        {
            UserEntity user = new UserEntity();
            user.Id = (int)reader["Id"];
            user.FirstName = (string)reader["FirstName"];
            user.LastName = (string)reader["LastName"];
            user.Login = (string)reader["Login"];
            user.Password = (string)reader["Password"];
            user.Email = (string)reader["Email"];
            user.Phone = reader["Phone"].ToString();
            user.IsDisabled = (bool)reader["Disabled"];
            var code = reader["ActivationCode"];
            if (code == null || code.ToString() == string.Empty)
            {
                user.ActivationCode = Guid.Empty;
            }
            else
            {
                user.ActivationCode = (Guid)code;
            }
            user.Roles = GetAllRolesValueForUser((int)reader["Id"]);
            return user;
        }

        private string ReadRole(SqlDataReader reader)
        {
            return reader["Role"].ToString();
        }

        private List<int> SetRoleId(List<string> roles)
        {
            List<int> roleId = new List<int>();
            foreach (var i in roles)
            {
                switch (i)
                {
                    case "Master": roleId.Add(1); break;
                    case "Moderator": roleId.Add(2); break;
                    case "Editor": roleId.Add(3); break;
                    case "User": roleId.Add(4); break;
                }
            }
            return roleId;
        }

        private string CreateHash(string unhashedPassword)
        {
            System.Security.Cryptography.MD5CryptoServiceProvider x = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] data = System.Text.Encoding.ASCII.GetBytes(unhashedPassword);
            data = x.ComputeHash(data);
            return System.Text.Encoding.ASCII.GetString(data);
        }

        #endregion
    }
}