﻿using BeStyle.Entities;
using BeStyle.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeStyle.Repositories.Sql
{
    public class OrderRepository : IOrderRepository
    {
        #region Fields
        private readonly string _connectionString;
        #endregion

        #region Queryies
        private const string INSERT_ORDER = "INSERT INTO [tblOrder] (UserId, Status, OrderDate) VALUES (@userId, 0, GETDATE());\n\rSELECT SCOPE_IDENTITY();";
        private const string INSERT_ITEM_FOR_ORDER = "INSERT INTO[tblOrderItem] (ProductId, SizeId, OrderId, Quantity, LogoFrontId, LogoFrontScale, LogoBackId, LogoBackScale) VALUES(@ProductId, @SizeId, @OrderId, @Quantity, @LogoFrontId, @LogoFrontScale, @LogoBackId, @LogoBackScale)";
        private const string UPDATE_SALE_FOR_PRODUCT = "UPDATE [tblProductSale] SET [SaleQuantity]=[SaleQuantity]+@SaleQuantity WHERE [ProductId]=@Id";
        private const string UPDATE_STATUS_FOR_ORDER = "UPDATE [tblOrder] SET [Status]=@Status WHERE [Id]=@Id";
        private const string GET_ITEMS_FOR_ORDER = "SELECT * FROM [tblOrderItem] WHERE [OrderId]=@Id";
        private const string GET_ORDERS = "SELECT * FROM tblOrder";
        #endregion

        #region Constructor

        public OrderRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        #endregion

        #region IOrderRepository

        public void SubmitAllOrder(int userId, List<OrderEntity> orderItems)
        {
            int orderId = 0;
            SqlConnection connection = new SqlConnection(_connectionString);
            SqlCommand cmd1 = new SqlCommand(INSERT_ORDER, connection);
            SqlCommand cmd2 = new SqlCommand(INSERT_ITEM_FOR_ORDER, connection);
            SqlCommand cmd3 = new SqlCommand(UPDATE_SALE_FOR_PRODUCT, connection);

            SqlTransaction transaction = null;
            try
            {
                connection.Open();
                transaction = connection.BeginTransaction();
                cmd1.Transaction = transaction;
                cmd2.Transaction = transaction;
                cmd3.Transaction = transaction;
                cmd1.Parameters.AddWithValue("userId", userId);
                orderId = (int)(decimal)cmd1.ExecuteScalar();
                foreach (var item in orderItems)
                {
                    cmd2.Parameters.Clear();
                    cmd3.Parameters.Clear();
                    cmd2.Parameters.AddWithValue("OrderId", orderId);
                    cmd2.Parameters.AddWithValue("SizeId", item.SizeId);
                    cmd2.Parameters.AddWithValue("Quantity", item.Quantity);
                    cmd2.Parameters.AddWithValue("ProductId", item.ProductId);
                    cmd2.Parameters.AddWithValue("LogoFrontId", (item.LogoFrontId == null) ? (object)DBNull.Value : item.LogoFrontId);
                    cmd2.Parameters.AddWithValue("LogoFrontScale", (item.LogoFrontScale == null) ? (object)DBNull.Value : item.LogoFrontScale);
                    cmd2.Parameters.AddWithValue("LogoBackId", (item.LogoBackId == null) ? (object)DBNull.Value : item.LogoBackId);
                    cmd2.Parameters.AddWithValue("LogoBackScale", (item.LogoBackScale == null) ? (object)DBNull.Value : item.LogoBackScale);

                    cmd3.Parameters.AddWithValue("SaleQuantity", item.Quantity);
                    cmd3.Parameters.AddWithValue("Id", item.ProductId);
                    cmd2.ExecuteNonQuery();
                    cmd3.ExecuteNonQuery();
                }
                transaction.Commit();
            }
            catch
            {
                transaction.Rollback();
            }
            finally
            {
                connection.Close();
            }
        }

        public List<SubmitEntity> GetAllSubmits()
        {
            List<SubmitEntity> submits = new List<SubmitEntity>();
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(GET_ORDERS, connection))
                {
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            submits.Add(ReadSubmit(reader));
                        }
                    }
                }
            }
            return submits;
        }

        private SubmitEntity ReadSubmit(SqlDataReader reader)
        {
            SubmitEntity submit = new SubmitEntity();
            submit.Id = (int)reader["Id"];
            submit.Time = (DateTime)reader["OrderDate"];
            submit.Status = (bool)reader["Status"];
            submit.User = new UserRepository(_connectionString).GetUserById((int)reader["UserId"]);
            return submit;
        }

        public void UpdateOrderStatus(int orderId, bool isChecked)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(UPDATE_STATUS_FOR_ORDER, connection))
                {
                    cmd.Parameters.AddWithValue("Id", orderId);
                    cmd.Parameters.AddWithValue("Status", isChecked);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public List<OrderEntity> GetAllOrdersForSubmit(int submitOrderId)
        {
            List<OrderEntity> orders = new List<OrderEntity>();
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(GET_ITEMS_FOR_ORDER, connection))
                {
                    cmd.Parameters.AddWithValue("Id", submitOrderId);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            orders.Add(ReadOrder(reader));
                        }
                    }
                }
            }
            return orders;
        }

        #endregion

        #region Helpers

        private OrderEntity ReadOrder(SqlDataReader reader)
        {
            OrderEntity orderItem = new OrderEntity();
            orderItem.Id = (int)reader["Id"];
            orderItem.ProductId = (int)reader["ProductId"];
            orderItem.LogoFrontId = (reader["LogoFrontId"] == DBNull.Value) ? null : (int?)reader["LogoFrontId"];
            orderItem.LogoBackId = (reader["LogoBackId"] == DBNull.Value) ? null : (int?)reader["LogoBackId"];
            orderItem.SizeId = (int)reader["SizeId"];
            orderItem.Quantity = (int)reader["Quantity"];
            orderItem.LogoFrontScale = (reader["LogoFrontScale"] == DBNull.Value) ? null : (int?)reader["LogoFrontScale"];
            orderItem.LogoBackScale = (reader["LogoBackScale"] == DBNull.Value) ? null : (int?)reader["LogoBackScale"];

            return orderItem;
        }

        #endregion
    }
}