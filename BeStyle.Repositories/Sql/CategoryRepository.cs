﻿using BeStyle.Entities;
using BeStyle.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeStyle.Repositories.Sql
{
    public class CategoryRepository:ICategoryRepository
    {
        #region Private Fields

        private readonly string _connectionString;
        private const string GET_CATEGORY_BY_ID = "SELECT [Id],[Category] FROM tblCategory WHERE Id=@Id";
        private const string GET_ALL_CATEGORY = @"SELECT [Id],[Category] FROM tblCategory ORDER BY Id";
        private const string UPDATE_QUERY = @"UPDATE tblCategory SET Category=@Category WHERE Id=@Id";
        private const string DELETE_BY_ID_QUERY = @"DELETE FROM tblCategory WHERE Id=@Id";
        private const string INSERT_QUERY = "INSERT INTO tblCategory(Category) VALUES(@Category)";

        #endregion

        #region Constructors

        public CategoryRepository(string connectionString)
        {
            this._connectionString = connectionString;
        }

        #endregion

        #region ICategoryRepository

        public string GetCategoryValueById(int id)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(GET_CATEGORY_BY_ID, connection))
                {
                    cmd.Parameters.AddWithValue("Id", id);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        reader.Read();
                        return (string)reader["Category"];
                    }
                }
            }
        }

        public List<CategoryEntity> GetAllCategory()
        {
            var categories = new List<CategoryEntity>();
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(GET_ALL_CATEGORY, connection))
                {
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var category = new CategoryEntity()
                            {
                                Category = (string)reader["Category"],
                                Id = (int)reader["Id"]
                            };
                            categories.Add(category);
                        }
                    }
                }
            }
            return categories.Count == 0 ? null : categories;
        }

        public void UpdateCategory(CategoryEntity category)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(UPDATE_QUERY, connection))
                {
                    cmd.Parameters.Add("@Id", SqlDbType.Int).Value = category.Id;
                    cmd.Parameters.Add("@Category", SqlDbType.NVarChar).Value = category.Category;
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void DeleteCategory(int Id)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (var cmd = new SqlCommand(DELETE_BY_ID_QUERY, connection))
                {
                    cmd.Parameters.AddWithValue("Id", Id);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void CreateCategory(CategoryEntity category)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(INSERT_QUERY, connection))
                {
                    cmd.Parameters.Add("@Category", SqlDbType.NVarChar).Value = category.Category;
                    cmd.ExecuteNonQuery();
                }
            }
        }

        #endregion

        #region Helpers

        private CategoryEntity ReadCategory(SqlDataReader reader)
        {
            CategoryEntity category = new CategoryEntity();
            category.Id = (int)reader["Id"];
            category.Category = (string)reader["Category"];
            return category;
        }

        #endregion
    }
}
