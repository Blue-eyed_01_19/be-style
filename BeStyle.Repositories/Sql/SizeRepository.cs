﻿using BeStyle.Entities;
using BeStyle.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeStyle.Repositories.Sql
{
    public class SizeRepository:ISizeRepository
    {
        #region Private Fields

        private readonly string _connectionString;
        private const string GET_ALL_QUERY = "SELECT [Id],[Size] FROM [tblSize]";
        private const string GET_SIZE_BY_ID = "SELECT[Id],[Size] FROM [tblSize] WHERE [Id]=@Id";

        #endregion

        #region Constructors

        public SizeRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        #endregion

        #region ISizeRepository

        public SizeEntity GetSizeValueById(int id)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(GET_SIZE_BY_ID, connection))
                {
                    cmd.Parameters.AddWithValue("Id", id);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        reader.Read();
                        SizeEntity size = new SizeEntity();
                        size.Id = id;
                        size.Size = (string)reader["Size"];
                        return size;
                    }                    
                }
            }
        }

        public List<SizeEntity> GetSizes()
        {
            List<SizeEntity> sizes = new List<SizeEntity>();
            using(SqlConnection connection=new SqlConnection(_connectionString))
            {
                connection.Open();
                using(SqlCommand cmd=new SqlCommand(GET_ALL_QUERY,connection))
                {
                    using(SqlDataReader reader=cmd.ExecuteReader())
                    {
                        while(reader.Read())
                        {
                            sizes.Add(ReadSize(reader));
                        }
                        return sizes;
                    }
                }
            }
        }

        #endregion

        #region Helpers

        private SizeEntity ReadSize(SqlDataReader reader)
        {
            SizeEntity size=new SizeEntity();
            size.Id=(int)reader["Id"];
            size.Size=(string)reader["Size"];
            return size;
        }

        #endregion
    }
}