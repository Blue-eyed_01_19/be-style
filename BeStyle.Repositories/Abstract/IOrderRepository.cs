﻿using BeStyle.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeStyle.Repositories.Abstract
{
    public interface IOrderRepository
    {
        void SubmitAllOrder(int userId, List<OrderEntity> orderItems);
        void UpdateOrderStatus(int orderId, bool isChecked);
        List<SubmitEntity> GetAllSubmits();
        List<OrderEntity> GetAllOrdersForSubmit(int submitOrderId);
    }
}
