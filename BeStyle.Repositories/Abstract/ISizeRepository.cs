﻿using BeStyle.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeStyle.Repositories.Abstract
{
    public interface ISizeRepository
    {
        SizeEntity GetSizeValueById(int id);
        List<SizeEntity> GetSizes();
    }
}
