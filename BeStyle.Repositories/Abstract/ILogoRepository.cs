﻿using BeStyle.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeStyle.Repositories.Abstract
{
    public interface ILogoRepository
    {
        List<LogoEntity> GetAllLogos();
        void DeleteById(int id);        
        void CreateLogo(LogoEntity logo);
        LogoEntity GetLogoById(int logoId);
        int GetLastInsertedId();
        List<LogoEntity> GetLogosForCurrentUser(int UserId);
    }
}
