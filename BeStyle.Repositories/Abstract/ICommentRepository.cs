﻿using BeStyle.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeStyle.Repositories.Abstract
{
    public interface ICommentRepository
    {
        List<CommentEntity> GetCommentsForProduct(int productId);
        List<CommentEntity> GetComments();
        void SetComment(int userId, int productId, string comment);
    }
}
