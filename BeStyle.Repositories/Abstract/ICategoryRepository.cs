﻿using BeStyle.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeStyle.Repositories.Abstract
{
    public interface ICategoryRepository
    {
        string GetCategoryValueById(int id);
        List<CategoryEntity> GetAllCategory();
        void UpdateCategory(CategoryEntity category);
        void DeleteCategory(int Id);
        void CreateCategory(CategoryEntity category);
    }
}
