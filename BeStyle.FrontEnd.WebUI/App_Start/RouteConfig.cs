﻿using System.Web.Mvc;
using System.Web.Routing;

namespace BeStyle.FrontEnd.WebUI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute("Constructor", "Constructor", new { controller = "Constructor", action = "Index" });
            routes.MapRoute("Contact", "Contact", new { controller = "Contact", action = "Contact" });
            routes.MapRoute("Cart", "Cart", new { controller = "Cart", action = "CartSummary" });
            routes.MapRoute("Profile", "Profile", new { controller = "Account", action = "UserProfile" });
            routes.MapRoute("Login", "Login", new { controller = "Account", action = "Login" });
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Shop", action = "Shop", id = UrlParameter.Optional }
            );
        }
    }
}
