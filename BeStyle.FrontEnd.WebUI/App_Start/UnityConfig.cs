using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Unity.Mvc5;
using System.Web.Configuration;
using BeStyle.FrontEnd.WebUI.Code.EmailManager;
using BeStyle.Repositories.Abstract;
using BeStyle.Repositories.Sql;
using BeStyle.WebUI.Security;
using NLog;

namespace BeStyle.FrontEnd.WebUI
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();            
            string connectionString=WebConfigurationManager.ConnectionStrings["BeStyleDBConnectionString"].ConnectionString;
            container.RegisterType<IProductRepository, ProductRepository>(new InjectionConstructor(connectionString));
            container.RegisterType<ICategoryRepository, CategoryRepository>(new InjectionConstructor(connectionString));
            container.RegisterType<ISizeRepository, SizeRepository>(new InjectionConstructor(connectionString));
            container.RegisterType<IOrderRepository, OrderRepository>(new InjectionConstructor(connectionString));
            container.RegisterType<ICommentRepository, CommentRepository>(new InjectionConstructor(connectionString));
            container.RegisterType<IUserRepository, UserRepository>(new InjectionConstructor(connectionString));
            container.RegisterType<ISecurityManager, SecurityManager>(new InjectionConstructor(connectionString));            
            container.RegisterType<ILogger, Logger>(new InjectionFactory(f => LogManager.GetCurrentClassLogger()));
            container.RegisterType<ILogoRepository, LogoRepository>(new InjectionConstructor(connectionString));
            container.RegisterType<IEmailManager, EmailManager>();
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}