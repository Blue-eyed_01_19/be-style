﻿using BeStyle.Entities;
using BeStyle.FrontEnd.WebUI.Models;
using BeStyle.Repositories.Sql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Configuration;
using BeStyle.Repositories.Abstract;
using BeStyle.WebUI.Security;
using System.Text;
using BeStyle.FrontEnd.WebUI.Code;

namespace BeStyle.FrontEnd.WebUI.Controllers
{
    public class CartController : Controller
    {
        #region Private Fields

        private readonly IProductRepository _productRepository;
        private readonly ISizeRepository _sizeRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly ISecurityManager _securityManager;
        private readonly ILogoRepository _logoRepository;

        #endregion

        #region Constructors

        public CartController(IProductRepository productRepository,
            ISizeRepository sizeRepository,
            IOrderRepository orderRepository,
            ISecurityManager securityManager,
            ILogoRepository logoRepository)
        {
            this._productRepository = productRepository;
            this._sizeRepository = sizeRepository;
            this._orderRepository = orderRepository;
            this._securityManager = securityManager;
            this._logoRepository = logoRepository;
        }

        #endregion

        #region Web Actions

        public ActionResult CartSummary()
        {
            return View(new CartModel
            {
                Cart = GetCart(),
                ReturnUrl = null
            });
        }

        [HttpPost]
        public ActionResult SubmitOrder()
        {
            if (!_securityManager.IsAuthenticated)
            {
                var result = new { Message = "Unauthentificated users have to SIGN IN before submitting" };
                return Json(result);
            }
            List<OrderEntity> items = new List<OrderEntity>();
            int userId = _securityManager.User.UserId;
            int countItems = GetCart().Items.Count();
            if (countItems!= 0)
            {
                foreach (var line in GetCart().Items)
                {
                    OrderEntity order = new OrderEntity();
                    order.ProductId = line.Product.Id;
                    order.SizeId = line.Size.Id;
                    order.Quantity = line.Quantity;
                    order.LogoFrontId = (line.LogoFront != null) ? line.LogoFront.Id : null;
                    order.LogoFrontScale = (line.LogoFrontScale != null) ? line.LogoFrontScale : null;
                    order.LogoBackId = (line.LogoBack != null) ? line.LogoBack.Id : null;
                    order.LogoBackScale = (line.LogoBackScale != null) ? line.LogoBackScale : null;
                    items.Add(order);
                }
                _orderRepository.SubmitAllOrder(userId, items);
                GetCart().Clear();
                return Json(new { Message = GetCart().Items.Count().ToString() + " items has been ordered!", Status = true });
            }
            else
            {
                return Json(new { Message = "Add products to cart before submitting!", Status = false });
            }
        }        

        [HttpPost]
        public ActionResult AddToCart(
            int productId,
            int sizeId,
            int? logoFrontId,
            int logoFrontScale,
            int? logoBackId,
            int logoBackScale,
            string userFrontImage,
            string userBackImage)

        {
            var product = _productRepository.GetProductById(productId);
            var size = _sizeRepository.GetSizeValueById(sizeId);
            LogoEntity logoFront = new LogoEntity();
            LogoEntity logoBack = new LogoEntity();

            if (userFrontImage != null)
            {
                logoFront.Picture = Convert.FromBase64String(FixBase64ForImage(userFrontImage));
                logoFront.IsUser = true;
                _logoRepository.CreateLogo(logoFront);
                logoFront.Id = _logoRepository.GetLastInsertedId();
            }
            else if (logoFrontId != 0)
            {
                logoFront = _logoRepository.GetLogoById((int)logoFrontId);
            }

            if (userBackImage != null)
            {
                logoBack.Picture = Convert.FromBase64String(FixBase64ForImage(userBackImage));
                logoBack.IsUser = true;
                _logoRepository.CreateLogo(logoBack);
                logoBack.Id = _logoRepository.GetLastInsertedId();
            }
            else if (logoBackId != 0)
            {
                logoBack = _logoRepository.GetLogoById((int)logoBackId);
            }

            GetCart().AddItem(product, size, logoFront, logoFrontScale, logoBack, logoBackScale, 1);

            return Json(new { Message = product.Name + "(Size:" + size.Size + ")  has been added succesfully!", Color = "Orange" });
        }

        [HttpPost]
        public ActionResult RemoveFromCart(int productId, int sizeId)
        {
            ProductEntity product = _productRepository.GetProductById(productId);
            GetCart().RemoveItem(productId, sizeId);
            var result = new
            {
                Message = product.Name + " has been removed from your cart!",
                DeleteId = productId,
                SizeId = sizeId,
                ItemCount = GetCart().Items.Where(p => p.Product.Id == productId).Count(),
                Total = GetCart().CalcTotalPrice()
            };
            return Json(result);
        }

        public RedirectToRouteResult ClearCart()
        {
            GetCart().Clear();
            return RedirectToAction("CartSummary", "Cart");
        }

        #endregion

        #region Helpers

        private Cart GetCart()
        {
            var cart = (Cart)Session[SessionKeys.CART_SESSION_KEY];            
            if (cart == null)
            {
                cart = new Cart();
                Session["Cart"] = cart;
            }
            
            return cart;
        }

        private string FixBase64ForImage(string image)
        {
            StringBuilder sbText = new StringBuilder(image, image.Length);
            sbText.Replace("\r\n", String.Empty);
            sbText.Replace(" ", String.Empty);
            return sbText.ToString();
        }

        #endregion
    }
}