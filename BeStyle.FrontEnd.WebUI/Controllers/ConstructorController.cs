﻿using BeStyle.Repositories.Abstract;
using BeStyle.WebUI.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BeStyle.FrontEnd.WebUI.Models;
using BeStyle.Entities;
using System.Text;

namespace BeStyle.FrontEnd.WebUI.Controllers
{
    public class ConstructorController : Controller
    {
        #region Private Fields

        private readonly IProductRepository _productRepository;
        private readonly ISizeRepository _sizeRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly ISecurityManager _securityManager;
        private readonly ILogoRepository _logoRepository;

        #endregion

        #region Constructors

        public ConstructorController(
            IProductRepository productRepository,
            ISizeRepository sizeRepository,
            ICategoryRepository categoryRepository,
            ISecurityManager securityManager,
            ILogoRepository logoRepository)
        {
            this._productRepository = productRepository;
            this._categoryRepository = categoryRepository;
            this._sizeRepository = sizeRepository;
            this._securityManager = securityManager;
            this._logoRepository = logoRepository;
        }

        #endregion

        #region Web Actions

        public ActionResult Index()
        {
            ConstructorModel constructor = new ConstructorModel();
            constructor.Products = _productRepository.GetProductsAll();
            constructor.Sizes = _sizeRepository.GetSizes();
            constructor.Categories = _categoryRepository.GetAllCategory();
            constructor.Logos = _logoRepository.GetAllLogos();
            constructor.CurrentProduct = GetCurrentProduct();
            if (_securityManager.IsAuthenticated)
            {
                constructor.UserLogos = _logoRepository.GetLogosForCurrentUser(_securityManager.User.UserId);
            }

            return View(constructor);
        }

        #endregion

        #region Helpers

        private ProductEntity GetCurrentProduct()
        {
            ProductEntity product = (ProductEntity)Session["Product"];
            if (product == null)
            {
                product = new ProductEntity();
                product = _productRepository.GetProductById(2);
                Session["Product"] = product;
            }
            return product;
        }

        #endregion
    }
}