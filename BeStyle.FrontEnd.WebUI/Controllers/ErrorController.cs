﻿using System;
using System.Web;
using System.Web.Mvc;
using NLog;

namespace BeStyle.FrontEnd.WebUI.Controllers
{
    public class ErrorController : Controller
    {
        #region Web Actions

        [HttpGet]
        public ActionResult Http404()
        {
            Response.StatusCode = 404;

            return View("Http404");
        }

        [HttpGet]
        public ActionResult Http500()
        {
            Response.StatusCode = 500;

            return View("Http500");
        }

        [HttpGet]
        public ActionResult Http500Ajax()
        {
            Response.StatusCode = 500;

            return PartialView("_Http500Partial");
        }

        #endregion
    }
}