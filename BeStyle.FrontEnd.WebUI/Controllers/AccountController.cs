﻿using System;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using Antlr.Runtime;
using BeStyle.Entities;
using BeStyle.FrontEnd.WebUI.Code.EmailManager;
using BeStyle.FrontEnd.WebUI.Models.AccountModels;
using BeStyle.FrontEnd.WebUI.Resources;
using BeStyle.Repositories.Abstract;
using BeStyle.Repositories.Enums;
using BeStyle.WebUI.Security;


namespace BeStyle.FrontEnd.WebUI.Controllers
{
    public class AccountController : Controller
    {
        #region Private Fields

        private readonly ISecurityManager _securityManager;
        private readonly IUserRepository _userRepository;
        private readonly IEmailManager _emailManager;

        #endregion

        #region Constructors

        public AccountController(ISecurityManager securityManager, IUserRepository userRepository, IEmailManager emailManager)
        {
            _securityManager = securityManager;
            _userRepository = userRepository;
            _emailManager = emailManager;
        }

        #endregion

        #region WebActions

        [HttpGet]
        public ActionResult Login(string returnUrl)
        {
            if (returnUrl != null)
            {
                ViewBag.ReturnUrl = returnUrl;
            }
            if (!_securityManager.IsAuthenticated)
            {
                var model = new AccountModel(); ;
                return View(model);
            }
            else
            {
                return RedirectToAction("Shop", "Shop");
            }
        }

        [HttpPost]
        public ActionResult Login(AccountModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            if (!_securityManager.IsAuthenticated)
            {
                if (!_securityManager.Login(model.Login.UserName, model.Login.Password, model.Login.RememberMe))
                {
                    return View();
                }
            }
            if (returnUrl != null)
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Shop", "Shop");
        }

        [HttpGet]
        public ActionResult LogOut(string returnUrl)
        {
            _securityManager.Logout();
            if (returnUrl != null)
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Shop", "Shop");
        }

        [HttpPost]
        [MvcReCaptcha.CaptchaValidator]
        public ActionResult Registration(AccountModel model, bool? captchaValid)
        {
            if (captchaValid == null || !captchaValid.Value)
            {
                return Json(new {Status = false, Message = "Capcha is invalid"});
            }
            if (!ModelState.IsValid)
            {
                return View("Login");
            }
            model.Registration.ActivationCode = Guid.NewGuid();
            if (model.Registration.Password != model.Registration.PasswordConfirmation)
            {
                return Json(new { Status = false, Message = "Passwords doesn't match." });
            }
            var user = new UserEntity()
            {
                Login = model.Registration.Login,
                Password = model.Registration.Password,
                ActivationCode = model.Registration.ActivationCode,
                Email = model.Registration.Email,
                FirstName = model.Registration.FirstName,
                LastName = model.Registration.LastName,
                IsDisabled = true,
                Phone = model.Registration.Phone
            };
            user.Roles.Add("User");
            if (_userRepository.CreateUser(user))
            {
                var name = string.Format("{0} {1}", user.FirstName, user.LastName);
                _emailManager.SendActivationCode(user.Email, name, user.Login, user.ActivationCode);
            }
            else
            {
                return Json(new { Status = false, Message = "Login or email is busy." });
            }

            return
                Json(
                    new
                    {
                        Status = true,
                        Message = "You have been created your account. Please, activate it using your email."
                    });
        }

        [HttpGet]
        public ActionResult RestorePassword()
        {
            return PartialView("_RestorePasswordPartial");
        }

        [HttpPost]
        public ActionResult RestorePassword(RestorePasswordModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { Status = false, Message = "Incorrect email" });
            }
            var user = _userRepository.GetUserByEmail(model.Email);
            if (user == null)
            {
                return Json(new { Status = false, Message = "User wasn't found at the given email" });
            }
            var newPassword = GeneratePassword();
            var cryptedNewPassword = _securityManager.CryptPassword(newPassword);
            _userRepository.RestorePassword(user.Id, cryptedNewPassword);
            var name = string.Format("{0} {1}", user.FirstName, user.LastName);
            _emailManager.SendRestoredPassword(user.Email, name, newPassword);
            var msg = string.Format(@"Password has been sent at {0}.", user.Email);
            return Json(new { Status = true, Message = msg });
        }

        [HttpGet]
        public ActionResult ConfirmRegistration(string login, string code)
        {
            var user = _userRepository.GetUserByLogin(login);

            var model = new AccountActivationModel();
            model.IsActivated = user.ActivationCode.ToString().Equals(code, StringComparison.InvariantCultureIgnoreCase);
            if (model.IsActivated)
            {
                _userRepository.ActivateUser(user.Id);
                return View(model);
            }
            else
            {
                return RedirectToAction("Shop", "Shop");
            }
        }

        [Authorize]
        [HttpGet]
        public ActionResult UserProfile()
        {
            var userId = _securityManager.User.UserId;
            var user = _userRepository.GetUserById(userId);
            var model = new ProfileModel()
            {
                UserId = userId,
                Login = user.Login,
                Email = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Phone = user.Phone,
            };
            return View("Profile", model);
        }

        [HttpPost]
        public ActionResult UserProfile(ProfileModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("Profile", model);
            }
            var user = new UserEntity()
            {
                Id = model.UserId,
                Login = model.Login,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Email = model.Email,
                Phone = model.Phone,
            };

            var ret = _userRepository.UpdateMainUserInfo(user);
            switch (ret)
            {
                case UpdateUserInfo.Success:
                    _securityManager.UpdateLogin(user.Login);
                    return Json(new { Status = true, Message = "User data has been changed." });
                case UpdateUserInfo.LoginAndEmailIsBusy:
                    return Json(new { Status = false, Message = "Login and email is busy." });
                case UpdateUserInfo.LoginIsBusy:
                    return Json(new { Status = false, Message = "Login is busy." });
                case UpdateUserInfo.EmailIsBusy:
                    return Json(new { Status = false, Message = "Email is busy." });
                default:
                    return View("Profile", model);
            }
        }

        [HttpGet]
        public ActionResult ChangePasswordPartial()
        {           
            var model = new ChangePasswordModel()
            {
                UserId = _securityManager.User.UserId
            };
            return PartialView("_ChangePasswordPartial", model);
        }

        [HttpPost]
        public ActionResult ChangePasswordPartial(ChangePasswordModel model)
        {
            if (model.NewPassword != model.PasswordConfirmation)
            {
                return Json(new { Status = false, Message = "New passwords doesn't match." });
            }

            var user = _userRepository.GetUserById(model.UserId);
            var cryptedPassword = _securityManager.CryptPassword(model.Password);
            if (cryptedPassword != user.Password)
            {
                return Json(new { Status = false, Message = "Old password is invalid." });
            }
            var newCryptedPassword = _securityManager.CryptPassword(model.NewPassword);
            _userRepository.ChangePassword(model.UserId, newCryptedPassword);

            return Json(new { Status = true, Message = "User password was changed." });
        }

        #endregion

        #region Helpers

        private string GeneratePassword()
        {
            var rng = new Random(DateTime.Now.Millisecond);
            const string chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            var size = rng.Next(6, 21);
            var buffer = new char[size];

            for (var i = 0; i < size; i++)
            {
                buffer[i] = chars[rng.Next(chars.Length)];
            }
            return new string(buffer);
        }

        #endregion
    }
}