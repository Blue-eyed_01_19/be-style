﻿using BeStyle.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BeStyle.FrontEnd.WebUI.Models
{
    public class Cart:ICart
    {
        private List<CartItem> itemCollection = new List<CartItem>();

        public void AddItem(ProductEntity product, SizeEntity size, LogoEntity logoFront, int? logoFrontScale, LogoEntity logoBack, int? logoBackScale, int quantity)
        {
            CartItem item = itemCollection.Where(p => p.Product.Id == product.Id
                                                   && p.Size.Id == size.Id).FirstOrDefault();
            if (item == null)
            {
                itemCollection.Add(new CartItem
                {
                    Product = product,
                    Size = size,
                    LogoFront = logoFront,
                    LogoFrontScale = logoFrontScale,
                    LogoBack = logoBack,
                    LogoBackScale = logoBackScale,
                    Quantity = 1
                });
            }
            else
            {
                item.Quantity += quantity;
            }
        }

        public void RemoveItem(int productId, int sizeId)
        {
            itemCollection.RemoveAll(i => i.Product.Id == productId && i.Size.Id == sizeId);
        }

        public decimal CalcTotalPrice()
        {
            return itemCollection.Sum(s => s.TotalPrice);
        }

        public void Clear()
        {
            itemCollection.Clear();
        }

        public virtual IEnumerable<CartItem> Items
        {
            get { return itemCollection; }
        }

    }
}