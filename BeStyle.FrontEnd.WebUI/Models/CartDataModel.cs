﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BeStyle.FrontEnd.WebUI.Models
{
    public class CartDataModel
    {
        public int productId;
        public int sizeId;
        public int? logoFrontId;
            public int logoFrontScale;
            public int? logoBackId;
            public int logoBackScale;
            public string userFrontImage;
            public string userBackImage;
    }
}