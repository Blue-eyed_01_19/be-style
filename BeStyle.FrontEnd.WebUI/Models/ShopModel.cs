﻿using BeStyle.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BeStyle.FrontEnd.WebUI.Models
{
    public class ShopModel
    {
        public List<ProductEntity> Products { get; set; }
        public IList<ProductEntity> TopProducts { get; set; }
        public List<SizeEntity> Sizes { get; set; }
        public List<CategoryEntity> Categories { get; set; }
        public List<CommentEntity> Comments { get; set; }
        public decimal MinPrice { get; set; }
        public decimal MaxPrice { get; set; }

    }
}