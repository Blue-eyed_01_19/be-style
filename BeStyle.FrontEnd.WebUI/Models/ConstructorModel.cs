﻿using BeStyle.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BeStyle.FrontEnd.WebUI.Models
{
    public class ConstructorModel
    {
        public List<ProductEntity> Products { get; set; }
        public List<SizeEntity> Sizes { get; set; }
        public List<CategoryEntity> Categories { get; set; }
        public List<LogoEntity> Logos { get; set; }
        public ProductEntity CurrentProduct { get; set; }
        public List<LogoEntity> UserLogos { get; set; }
    }
}