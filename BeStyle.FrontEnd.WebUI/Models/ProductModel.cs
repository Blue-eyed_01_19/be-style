﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BeStyle.Entities;

namespace BeStyle.FrontEnd.WebUI.Models
{
    public class ProductModel
    {
        public ProductEntity Product { get; set; }

        public List<SizeEntity> Sizes { get; set; }

        public List<CommentEntity> Comments { get; set; }

        public bool IsAuthenticated { get; set; }
    }
}