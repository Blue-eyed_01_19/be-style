﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BeStyle.FrontEnd.WebUI.Models
{
    public class CartModel
    {
        public Cart Cart { get; set; }
        public string ReturnUrl { get; set; }
    }
}