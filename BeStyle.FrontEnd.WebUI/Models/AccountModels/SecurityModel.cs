﻿namespace BeStyle.FrontEnd.WebUI.Models.AccountModels
{
    public class SecurityModel
    {
        public string UserName { get; set; }

        public bool IsAuthenticated { get; set; }

        public int UserId { get; set; }
    }
}