﻿using BeStyle.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BeStyle.FrontEnd.WebUI.Models
{
    public class CartItem
    {
        public ProductEntity Product { get; set; }
        public SizeEntity Size { get; set; }
        public int Quantity { get; set; }
        public LogoEntity LogoFront { get; set; }
        public int? LogoFrontScale { get; set; }
        public LogoEntity LogoBack { get; set; }
        public int? LogoBackScale { get; set; }
        public decimal TotalPrice
        {
            get { return Product.Price * Quantity; }
        }
    }
}