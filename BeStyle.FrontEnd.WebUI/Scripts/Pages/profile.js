﻿(function ($) {
    var ProfilePage = function () {
        var initializeEvents = function() {
            $("#change-password").click(function (e) {
                e.preventDefault();
                $.get(this.href, function (data) {
                    $('#dialogContent').html(data);
                    $('#modDialog').modal("show");
                    $.validator.unobtrusive.parse("#change-password-form");
                    $("#btn-close").click(function (event) {
                        event.preventDefault();
                        $("#modDialog").modal("hide");
                    });
                });
            });
        };

        window.profileChanged = function (content) {
            if (content.Status === false) {
                toastr["error"](content.Message, "Error!");
            } else {
                location.reload();
                toastr["success"](content.Message, "Success!");
            }
        };

        window.passwordChanged = function (content) {
            if (content.Status === false) {
                toastr["error"](content.Message, "Error!");
            } else {
                $('#modDialog').modal("hide");
                toastr["success"](content.Message, "Success!");
            }
        };

        this.initialize = function () {
            initializeEvents();
        };
    };

    $(document).ready(function () {
        var page = new ProfilePage();
        page.initialize();
    });

})(window.jQuery);