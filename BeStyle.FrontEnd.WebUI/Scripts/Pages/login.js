﻿(function ($) {

    var LoginPage = function () {

        window.formBegin = function () {
            showLoader();
        };

        window.registrationSuccess = function (content) {
            hideLoader();
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "0",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            if (content.Status === false) {
                toastr["error"](content.Message, "Error!");
            } else {
                toastr["success"](content.Message, "Success!");
            }
        };

        window.passwordRestored = function (content) {
            hideLoader();
            toastr.options = {
                "closeButton": true
            }
            if (content.Status === false) {
                toastr["error"](content.Message, "Error!");
            } else {
                toastr["success"](content.Message, "Success!");
                $("#modDialog").modal("hide");
            }
        };

        var initializeEvents = function () {
            $("#restore-password").click(function (e) {
                e.preventDefault();
                $.get(this.href, function (data) {
                    $('#dialogContent').html(data);
                    $('#modDialog').modal("show");
                    $.validator.unobtrusive.parse("#restore-password-form");
                    $("#btn-close").click(function (event) {
                        event.preventDefault();
                        $("#modDialog").modal("hide");
                    });
                });
            });
        };

        this.initialize = function () {
            initializeEvents();
        };
    };

    $(document).ready(function () {
        var page = new LoginPage();
        page.initialize();
    });
})(window.jQuery);