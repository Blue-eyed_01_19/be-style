(function ($) {
    var ConstructorPage = function () {
        var selectedPicture;
        var productId;
        var productPrice;
        var logoId;
        var sizeId;
        var logoScale;
        var selectedPictureWidth;
        var selectedPictureHeight;
        var selectedProductCategory;

        var isBack;
        var logoFrontId;
        var logoBackId = 0;
        var logoFrontScale = 0;
        var logoBackScale = 0;
        var logoFront;
        var logoBack;
        var isUserFront;
        var isUserBack;
        var userFrontImage = null;
        var userBackImage = null;

        var deletePicture = function () {
            if (selectedPicture != null) {
                var imgPosition = document.getElementById('imgPositionCloser');
                imgPosition.removeChild(selectedPicture);
                selectedPicture = null;
                document.getElementById('logoScale').textContent = "Logo: ";
                if (isBack) {
                    logoBackScale = logoScale;
                    logoBackId = 0;
                }
                else {
                    logoFrontScale = logoScale;
                    logoFrontId = 0;
                }
            }
        }

        var zoomIn = function () {
            if (selectedPicture != null) {
                logoScale = logoScale + 5;
                selectedPicture.setAttribute("width", selectedPictureWidth * logoScale / 100);
                selectedPicture.setAttribute("height", selectedPictureHeight * logoScale / 100);
                document.getElementById('logoScale').textContent = "Logo: " + logoScale + "%";
            }
            if (isBack) {
                logoBackScale = logoScale;
            }
            else {
                logoFrontScale = logoScale;
            }
        }

        var zoomOut = function () {
            if (selectedPicture != null) {
                if (logoScale > 5) {
                    logoScale = logoScale - 5;
                }
                selectedPicture.setAttribute("width", selectedPictureWidth * logoScale / 100);
                selectedPicture.setAttribute("height", selectedPictureHeight * logoScale / 100);
                document.getElementById('logoScale').textContent = "Logo: " + logoScale + "%";
            }
            if (isBack) {
                logoBackScale = logoScale;
            }
            else {
                logoFrontScale = logoScale;
            }
        }

        var getBase64Image = function (img) {
            if (img != null) {
                var canvas = document.createElement("canvas");
                canvas.width = img.width;
                canvas.height = img.height;

                var ctx = canvas.getContext("2d");
                ctx.drawImage(img, 0, 0);
                var dataURL = canvas.toDataURL("image/png");

                return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
            }
        }

        var resetParam = function () {
            var imgPosition = document.getElementById('imgPositionCloser');
            while (imgPosition.firstChild) {
                imgPosition.removeChild(imgPosition.firstChild);
            }
            isBack = false;
            logoScale = 100;
            logoId = 0;
            selectedPicture = 0;
            logoFront = 0;
            logoBack = 0;
            logoFrontId = 0;
            logoBackId = 0;
            isUserFront = false;
            isUserBack = false;
            userFrontImage = null;
            userBackImage = null;

            document.getElementById('logoScale').textContent = "Logo: " + logoScale + "%";
        }

        var initializeEvents = function () {
            $(".product-picture").click(function (event) {
                var mainImage = document.getElementById('imgMainClose');
                var mainImageFront = document.getElementById('productfront');
                var mainImageBack = document.getElementById('productback');
                var imgPosition = document.getElementById('imgPositionCloser');
                imgPosition.style.visibility = "visible";

                if (mainImage) {
                    mainImage.src = event.target.src;
                    mainImageFront.src = event.target.src;

                    productId = $(this).attr("data-product-id");
                    productPrice = $(this).attr("data-product-price");
                    selectedProductCategory = $(this).attr("data-product-category");
                    document.getElementById('productPrice').textContent = "$" + productPrice;
                    var productPictireBack = $(this).attr("data-product-back-picture");
                    mainImageBack.src = productPictireBack;
                }
                resetParam();
            });

            $("#productfront").click(function (event) {
                if (isBack) {
                    var mainImage = document.getElementById('imgMainClose');
                    if (mainImage) {
                        mainImage.src = event.target.src;
                    }
                    isBack = false;

                    if (logoBack) {
                        logoBack = selectedPicture;
                        logoBackId = logoId;
                    }
                    if (logoFront) {
                        logoScale = logoFrontScale;
                        logoId = logoFrontId;
                    }
                    else {
                        logoScale = "";
                    }

                    var imgPosition = document.getElementById('imgPositionCloser');

                    while (imgPosition.firstChild) {
                        imgPosition.removeChild(imgPosition.firstChild);
                    }

                    imgPosition.style.visibility = "visible";
                    document.getElementById('logoScale').textContent = "Logo: " + logoScale + "%";
                    if (imgPosition && logoFront) {
                        var elem = document.createElement("img");
                        elem = logoFront;

                        elem.addEventListener("click", function () {
                            if (selectedPicture) {
                                selectedPicture.style.border = 'none';
                            }
                            elem.style.border = '1px solid #E8272C';
                        });
                        selectedPicture = elem;
                        imgPosition.appendChild(elem);
                    }
                }
            });

            $("#productback").click(function (event) {
                if (!isBack) {
                    var mainImage = document.getElementById('imgMainClose');
                    var imgPosition = document.getElementById('imgPositionCloser');
                    if (mainImage) {
                        mainImage.src = event.target.src;
                    }
                    isBack = true;
                    if (logoFront) {
                        logoFront = selectedPicture;
                        logoFrontId = logoId;
                    }
                    if (logoBack) {
                        logoScale = logoBackScale;
                        logoId = logoBackId;
                    }
                    else {
                        logoScale = "";
                    }

                    var imgPosition = document.getElementById('imgPositionCloser');

                    while (imgPosition.firstChild) {
                        imgPosition.removeChild(imgPosition.firstChild);
                    }

                    document.getElementById('logoScale').textContent = "Logo: " + logoScale + "%";
                    if (selectedProductCategory == 4) {
                        imgPosition.style.visibility = "hidden";
                    }
                    if (imgPosition && logoBack) {
                        var elem = document.createElement("img");
                        elem = logoBack;

                        elem.addEventListener("click", function () {
                            if (selectedPicture) {
                                selectedPicture.style.border = 'none';
                            }
                            elem.style.border = '1px solid #E8272C';
                        });

                        selectedPicture = elem;

                        if (selectedProductCategory != 4) {
                            imgPosition.appendChild(elem);
                        } 
                    }
                }
            });

            $(".btn-size").click(function (event) {
                sizeId = $(this).attr("data-size-id");
            });

            $(".logo-picture").click(function (event) {
                if (selectedProductCategory == 4 && isBack) {
                    return;
                }
                var imgPosition = document.getElementById('imgPositionCloser');
                var item = event.target;
                logoScale = 100;
                document.getElementById('logoScale').textContent = "Logo: " + logoScale + "%";

                while (imgPosition.firstChild) {
                    imgPosition.removeChild(imgPosition.firstChild);
                }

                if (imgPosition) {
                    var elem = document.createElement("img");
                    elem.setAttribute("src", item.src);

                    if (elem.height / elem.width > 1) {
                        var beforeHeight = elem.height;
                        elem.setAttribute("height", 0.8 * imgPosition.clientHeight);
                        elem.setAttribute("width", elem.height / beforeHeight * elem.width);
                    }
                    else {
                        var beforeWidth = elem.width;
                        elem.setAttribute("width", 0.8 * imgPosition.clientWidth);
                        elem.setAttribute("height", elem.width / beforeWidth * elem.height);
                    }
                    selectedPicture = elem;
                    selectedPictureWidth = selectedPicture.width;
                    selectedPictureHeight = selectedPicture.height;

                    imgPosition.appendChild(elem);
                }

                logoId = $(this).attr("data-logo-id");

                if (isBack) {
                    logoBack = selectedPicture;
                    logoBackScale = logoScale;
                    logoBackId = logoId;
                    isUserBack = false;
                    userBackImage = null;
                }
                else {
                    logoFront = selectedPicture;
                    logoFrontScale = logoScale;
                    logoFrontId = logoId;
                    isUserFront = false;
                    userFrontImage = null;
                }
            });

            $(".zoomIn").click(function (event) {
                zoomIn();
            });

            $(".zoomOut").click(function (event) {
                zoomOut();
            });

            $(".deletePicture").click(function (event) {
                deletePicture();
            });

            $("body").keydown(function (event) {
                if (event.keyCode == 46) {
                    deletePicture();
                }

                if (event.keyCode == 107) {
                    zoomIn();
                }

                if (event.keyCode == 109) {
                    zoomOut();
                }
            });

            $(".addFromConstructor").click(function () {
                if (productId != null & sizeId != null) {
                    var oldValue = document.getElementById("cart-item-count").innerHTML;
                    var newValue = parseInt(oldValue) + 1;
                    document.getElementById("cart-item-count").innerHTML = newValue;
                    showLoader();
                    var model = {
                        productId: productId,
                        sizeId: sizeId,
                        logoFrontId: logoFrontId,
                        logoFrontScale: logoFrontScale,
                        logoBackId: logoBackId,
                        logoBackScale: logoBackScale,
                        userFrontImage: getBase64Image(userFrontImage),
                        userBackImage: getBase64Image(userBackImage)
                    };
                    $.ajax({
                        type: 'POST',
                        data: model,
                        url: '/Cart/AddToCart',
                        dataType: 'json',
                        //contentType: 'application/json; charset=utf-8',
                        success: function (data) {
                            hideLoader();
                            toastr["success"]("The proudct has been added to Shopping Cart!", "Success!");
                        }
                    });
                }
            });

            var control = document.getElementById("choseImageButton");
            $(control).change(function (event) {
                var files = control.files,
                len = files.length;
                imageUrl = URL.createObjectURL(files[len - 1]);
                var image = new Image();
                image.src = imageUrl;

                image.onload = function () {
                    var imgPosition = document.getElementById('imgPositionCloser');
                    logoScale = 100;
                    document.getElementById('logoScale').textContent = "Logo: " + logoScale + "%";

                    while (imgPosition.firstChild) {
                        imgPosition.removeChild(imgPosition.firstChild);
                    }

                    if (imgPosition) {
                        var elem = document.createElement("img");
                        elem.setAttribute("src", image.src);

                        if (elem.height / elem.width > 1) {
                            var beforeHeight = elem.height;
                            elem.setAttribute("height", 0.8 * imgPosition.clientHeight);
                            elem.setAttribute("width", elem.height / beforeHeight * elem.width);
                        }
                        else {
                            var beforeWidth = elem.width;
                            elem.setAttribute("width", 0.8 * imgPosition.clientWidth);
                            elem.setAttribute("height", elem.width / beforeWidth * elem.height);
                        }
                        selectedPicture = elem;
                        selectedPictureWidth = selectedPicture.width;
                        selectedPictureHeight = selectedPicture.height;

                        imgPosition.appendChild(elem);
                    }
                    if (isBack) {
                        logoBack = selectedPicture;
                        logoBackScale = logoScale;
                        logoBackId = 0;
                        isUserBack = true;
                        userBackImage = image;
                    }
                    else {
                        logoFront = selectedPicture;
                        logoFrontScale = logoScale;
                        logoFrontId = 0;
                        isUserFront = true;
                        userFrontImage = image;
                    }
                }
            });
        }

        this.initialize = function () {
            isBack = false;
            selectedPicture = null;
            productId = $('#imgMainClose').attr('data-current-pictire-id');
            initializeEvents();
        };
    };

    $(document).ready(function () {
        var page = new ConstructorPage();
        page.initialize();
    });
})(window.jQuery);