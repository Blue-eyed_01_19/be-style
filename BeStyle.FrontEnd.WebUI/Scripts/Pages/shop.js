﻿(function ($) {
    var ShopPage = function () {

        var initializeEvents = function () {

            $(".add-to-cart").click(function (e) {
                e.preventDefault();
                showLoader();
                $.get(this.href, function (data) {
                    $('#dialogContent').empty();
                    $('#dialogContent').html(data);
                    $('#modDialog').modal('show');
                })
                .done(function () {
                    hideLoader();
                    $("#btn-close").click(function (event) {
                        event.preventDefault();
                        $("#modDialog").modal("hide");
                    });
                    $(".AddLink").click(function () {
                        showLoader();
                        var recordProductId = $(this).attr("data-product-id");
                        var recordSizeId = $(".btn-size.active").attr("data-size-id");
                        var recordLogoFrontId = 0;
                        var recordLogoFrontScale = 0;
                        var recordLogoBackId = 0;
                        var recordLogoBackScale = 0;
                        var oldValue = document.getElementById("cart-item-count").innerHTML;
                        var newValue = parseInt(oldValue) + 1;
                        document.getElementById("cart-item-count").innerHTML = newValue;
                        $.post("/Cart/AddToCart", {
                            "productId": recordProductId,
                            "sizeId": recordSizeId,
                            "logoFrontId": recordLogoFrontId,
                            "logoFrontScale": recordLogoFrontScale,
                            "logoBackId": recordLogoBackId,
                            "logoBackScale": recordLogoBackScale
                        },
                            function (data) {
                                hideLoader();
                                $(".rows").html = "";
                                $("#cart-item-count").textContent = newValue;
                                toastr.options = {
                                    "closeButton": false,
                                    "debug": false,
                                    "newestOnTop": false,
                                    "progressBar": false,
                                    "positionClass": "toast-top-full-width",
                                    "preventDuplicates": false,
                                    "showDuration": "300",
                                    "hideDuration": "1000",
                                    "timeOut": "5000",
                                    "extendedTimeOut": "1000",
                                    "showEasing": "swing",
                                    "hideEasing": "linear",
                                    "showMethod": "fadeIn",
                                    "hideMethod": "fadeOut",
                                    "onclick": function() {
                                        window.location.href = "/Cart";
                                    }
                                };
                                toastr["success"](data.Message, "Success!");
                            });
                    });

                    $(".AddComment").click(function () {
                        showLoader();
                        var recordComment = $("#commentText").val();
                        var recordProductId = $(this).attr("data-product-id");
                        $.post("/Shop/AddComment", { "productId": recordProductId, "comment": recordComment },
                            function (data) {
                                hideLoader();
                                toastr["success"]("Comment succesfully added!", "Comment");
                                var total = $("#all-comment");
                                total.prepend(
                                "<ul><li><a href=''><i class='fa fa-user'>" + data.User +
                                "</i></a></li><li><a href=''><i class='fa fa-clock-o'>" + data.Time +
                                "</i></a></li><p>" + data.Comment + "</p></ul>"
                                    );
                            });
                    });
                    $('[data-toggle="tooltip"]').tooltip();
                });
            });

            $("#comments-state").click(function () {
                if ($(this).next('.accordion-body').hasClass('in')) {
                    $(this).text('Hide comments...');
                } else {
                    $(this).text('Show comments...');
                }
            });

            $(".SendMail").click(function () {
                var recordSubject = document.getElementById("subject-message").value;
                var recordText = document.getElementById("message").value;
                $.post("/Contact/SendEmailtoUs", { "subject": recordSubject, "text": recordText },
                    function (data) {
                        toastr.options =
                        {
                            closeButton: true
                        }
                        if (data.Status == true) {
                            toastr["success"](data.Message, "E-mail");
                        }
                        else {
                            toastr.options =
                            {
                                onclick: function () {
                                    var returnUrl = window.location.pathname;
                                    window.location.href = '/Account/Login/?returnUrl=' + returnUrl;
                                }
                            }
                            toastr["error"](data.Message, "E-Mail");
                        }
                    });
            });
        };

        var encode = function (data) {
            var str = String.fromCharCode.apply(null, data);
            return btoa(str).replace(/.{76}(?=.)/g, '$&\n');
        };

        // Read a page's GET URL variables and return them as an associative array.
        var getUrlVars = function () {
            var vars = [], hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        }

        var priceRange = function (start, end) {
            $("#slider-range").slider({
                range: true,
                min: start,
                max: end,
                values: [start, end],
                slide: function (event, ui) {
                    $("#amount-start").val("$" + $("#slider-range").slider("values", 0));
                    $("#amount-end").val("$" + $("#slider-range").slider("values", 1));
                },
                stop: function (event, ui) {
                    // ajax code here.
                    event.preventDefault();
                    $("#amount-start").val("$" + $("#slider-range").slider("values", 0));
                    $("#amount-end").val("$" + $("#slider-range").slider("values", 1));
                    var start = $("#slider-range").slider("values", 0);
                    var end = $("#slider-range").slider("values", 1);
                    var categoryId = getUrlVars()["categoryId"];
                    $.ajax({
                        type: 'GET',
                        url: '/Shop/ShopByPrice',
                        data: { start: start, end: end, categoryId: categoryId },
                        beforeSend: function () {
                            showLoader();
                        },
                        success: function (data) {
                            var $container = $("#products-container");
                            $container.empty();
                            $container.html(data);
                            initializeEvents();
                        },
                        complete: function () {
                            hideLoader();
                        },
                        error: function (data) {
                            hideLoader();
                            //                             $('#modDialog').css("width", "1480px");
                            //                             $('#dialogContent').css("width", "auto");
                            $('#dialogContent').empty();
                            $('#dialogContent').html(data.responseText);
                            $('#modDialog').modal('show');
                            $("#btn-close").click(function (e) {
                                e.preventDefault();
                                $("#modDialog").modal("hide");
                            });
                        }
                    });

                }
            });
            $("#amount-start").val("$" + $("#slider-range").slider("values", 0));
            $("#amount-end").val("$" + $("#slider-range").slider("values", 1));
        };

        this.initialize = function () {
            $.ajaxSetup({ cache: false });
            initializeEvents();
            var start = $("#amount-start").attr("data-price");
            var end = $("#amount-end").attr("data-price");
            priceRange(start, end);
        };
    };

    $(document).ready(function () {
        var page = new ShopPage();
        page.initialize();
    });
})(window.jQuery);