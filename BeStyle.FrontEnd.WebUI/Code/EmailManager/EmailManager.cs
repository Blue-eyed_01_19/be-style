﻿using System;
using System.Net;
using System.Net.Mail;
using System.Web;
using BeStyle.Entities;
using BeStyle.FrontEnd.WebUI.Resources;

namespace BeStyle.FrontEnd.WebUI.Code.EmailManager
{
    public class EmailManager : IEmailManager
    {
        #region IEmailManager

        public bool SendActivationCode(string to, string name, string login, Guid code)
        {
            var fromAddress = new MailAddress(BeStyleRes.Email, BeStyleRes.EmailDisplay);
            var toAddress = new MailAddress(to, name);

            var fromPassword = BeStyleRes.EmailPassword;
            var subject = BeStyleRes.RegistrationConfirmSubject;
            var url = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            url += "/Account/ConfirmRegistration?login=" + login + "&code=" + code;

            var body = string.Format(
                "<h2>Dear {0}!</h2><br/>" +
                "Thanks for your interest in our online shop. Please click the following link, to confirm registration.<br/><h4><a href=\"" +
                url + "\">Activate account</a></h4><br/>" +
                "<br/><br/>Best wishes,<br/>" +
                "The BeStyle Team.",
                name);

            var sc = new SmtpClient
            {
                Host = @"smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword),
                Timeout = 20000
            };

            using (var msg = new MailMessage())            
            {
                msg.IsBodyHtml = true;
                msg.From = fromAddress;
                msg.To.Add(toAddress);
                msg.Subject = subject;
                msg.Body = body;
                sc.Send(msg);
            }
            sc.Dispose();

            return true;
        }

        public bool SendRestoredPassword(string to, string name, string restoredPassword)
        {
            var fromAddress = new MailAddress(BeStyleRes.Email, BeStyleRes.EmailDisplay);
            var toAddress = new MailAddress(to, name);

            var fromPassword = BeStyleRes.EmailPassword;
            var subject = BeStyleRes.RestorePasswordSubject;

            var body = string.Format(
                "<h2>Dear {0}!</h2><br/>" +
                "Your new password is <h3>{1}</h3>. Please, change it as soon as possible in your personal cabinet." +
                "<br/><br/>Best wishes,<br/>" +
                "The BeStyle Team.",
                name, restoredPassword);

            var sc = new SmtpClient
            {
                Host = @"smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword),
                Timeout = 20000
            };

            using (var msg = new MailMessage())             
            {
                msg.IsBodyHtml = true;
                msg.From = fromAddress;
                msg.To.Add(toAddress);
                msg.Subject = subject;
                msg.Body = body;
                sc.Send(msg);
            }
            sc.Dispose();

            return true;
        }

        public bool SendMailToUs(string subject, string text, string fromDisplay)
        {
            var fromAddress = new MailAddress(BeStyleRes.Email, fromDisplay);
            var toAddress = new MailAddress(BeStyleRes.Email, BeStyleRes.EmailDisplay);
            var fromPassword = BeStyleRes.EmailPassword;
            var body = string.Format("I, {0}, send to you this message: <br/><br/> {1}", fromDisplay, text);
            var sc = new SmtpClient
            {
                Host = @"smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword),
                Timeout = 20000
            };
            using (var msg = new MailMessage())          
            {
                msg.IsBodyHtml = true;
                msg.From = fromAddress;
                msg.To.Add(toAddress);
                msg.Subject = subject;
                msg.Body = body;
                sc.Send(msg);
            }
            sc.Dispose();

            return true;
        }

        #endregion
    }
}