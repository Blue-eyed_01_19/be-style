﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BeStyle.Entities.Enums;

namespace BeStyle.Entities
{
    public class UserEntity
    {
        #region Constructors

        public UserEntity()
        {
            Roles = new List<string>();
        }

        #endregion

        #region Public Properties

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public List<string> Roles { get; set; }
        public Guid ActivationCode { get; set; }
        public string CommaSeparatedRoles
        {
            get
            {
                if (Roles == null || Roles.Count == 0)
                {
                    return string.Empty;
                }
                else
                {
                    var retValue = new StringBuilder();
                    foreach (var role in Roles)
                    {
                        retValue.Append(role);
                        retValue.Append(",");
                    }

                    return retValue.ToString(0, retValue.Length - 1);
                }
            }
        }
        public bool IsDisabled { get; set; }
        public bool IsMaster
        {
            get
            {
                return Roles != null && Roles.Count > 0 &&
                       Roles.Any(role => ((UserRoles)Enum.Parse(typeof(UserRoles), role)) == UserRoles.Master);

            }
        }
        public bool IsModerator
        {
            get
            {
                return Roles != null && Roles.Count > 0 &&
                    Roles.Any(role => ((UserRoles)Enum.Parse(typeof(UserRoles), role)) == UserRoles.Moderator);
            }
        }
        public bool IsEditor
        {
            get
            {
                return Roles != null && Roles.Count > 0 &&
                    Roles.Any(role => ((UserRoles)Enum.Parse(typeof(UserRoles), role)) == UserRoles.Editor);

            }
        }
        public bool IsUser
        {
            get
            {
                return Roles != null && Roles.Count > 0 &&
                    Roles.Any(role => ((UserRoles)Enum.Parse(typeof(UserRoles), role)) == UserRoles.User);

            }
        }

        #endregion
    }
}
