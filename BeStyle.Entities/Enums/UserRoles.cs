﻿namespace BeStyle.Entities.Enums
{
    public enum UserRoles
    {
        Master,
        Moderator,
        Editor,
        User
    }
}