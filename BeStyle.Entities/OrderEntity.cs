﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeStyle.Entities
{
    //public class OrderEntity
    //{
    //    public int Id { get; set; }        
    //    public int ProductId { get; set; }
    //    public int? LogoId { get; set; }
    //    public int SizeId { get; set; }
    //    public int OrderId { get; set; }
    //    public int Quantity { get; set; }
    //    public int? LogoScale { get; set; }
    //    public bool IsBack { get; set; }
    //}

    public class OrderEntity
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int SizeId { get; set; }
        public int OrderId { get; set; }
        public int Quantity { get; set; }
        public int? LogoFrontId { get; set; }
        public int? LogoFrontScale { get; set; }
        public int? LogoBackId { get; set; }
        public int? LogoBackScale { get; set; }
    }
}