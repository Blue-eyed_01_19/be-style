﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeStyle.Entities
{
    public class LogoEntity
    {
        public int? Id { get; set; }
        public byte[] Picture { get; set; }
        public bool IsUser { get; set; }
    }
}
