﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeStyle.Entities
{
    public class ProductSaleEntity
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int SaleQuantity { get; set; }

    }
}
