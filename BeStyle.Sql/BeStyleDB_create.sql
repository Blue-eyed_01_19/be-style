﻿GO
CREATE DATABASE BeStyleDB;

GO
USE BeStyleDB

CREATE TABLE tblUser
(
	Id INT PRIMARY KEY IDENTITY (1,1) NOT NULL,
	FirstName NVARCHAR(50),
	LastName NVARCHAR(50),
	Email NVARCHAR(50) NOT NULL UNIQUE,
	Phone NVARCHAR(50),
	Login NVARCHAR(50) NOT NULL UNIQUE,
	Password NVARCHAR(50) NOT NULL,
	Disabled BIT NOT NULL,
	ActivationCode UNIQUEIDENTIFIER NOT NULL DEFAULT NEWID()
	CONSTRAINT chk_Email CHECK (Email LIKE '%@%.%')
)

CREATE TABLE  tblRole
(
	Id INT PRIMARY KEY IDENTITY (1,1) NOT NULL,	
	Role NVARCHAR(50) UNIQUE
)

CREATE TABLE tblUserRole
(
	Id INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
	UserId INT NOT NULL,
	RoleId INT NOT NULL,
	CONSTRAINT FK_tblUserRole_UserId_tblUser_Id 
		FOREIGN KEY (UserId) 
			REFERENCES tblUser(Id),
	CONSTRAINT FK_tblUserRole_UserId_tblRole_Id 
		FOREIGN KEY (RoleId) 
			REFERENCES tblRole(Id)							
)

CREATE TABLE tblCategory
(
	Id INT PRIMARY KEY IDENTITY  (1,1)  NOT NULL,
	Category NVARCHAR(50) NOT NULL UNIQUE
)

CREATE TABLE tblProduct
(	
	Id INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
	Name NVARCHAR(50) NOT NULL,
	Description NVARCHAR(200),
	Price DECIMAL (10) NOT NULL,
	Picture VARBINARY(MAX),
	PictureBack VARBINARY(MAX),
	CategoryId INT NOT NULL,
	CONSTRAINT FK_tblProduct_CategoryId_tblCategory_Id
		FOREIGN KEY (CategoryId)
			REFERENCES tblCategory(Id)	
)

CREATE TABLE tblProductSale
(
	Id INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
	ProductId INT NOT NULL,
	SaleQuantity INT DEFAULT 0,
	CONSTRAINT FK_tblProductSale_ProductId_tblProduct_Id
		FOREIGN KEY (ProductId)
			REFERENCES tblProduct(Id)
			ON DELETE CASCADE
)

CREATE TABLE tblSize
(
	Id INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
	Size NVARCHAR(50) NOT NULL UNIQUE
)

CREATE TABLE tblLogo
(
	Id INT PRIMARY KEY IDENTITY (1,1) NOT NULL,
	Picture  VARBINARY(max) 
)

CREATE TABLE Logo_temp(
Id INT PRIMARY KEY IDENTITY (1,1) NOT NULL,
	Picture  VARBINARY(max) NULL,
	IsUser BIT DEFAULT 0 NOT NULL 
);
GO
-- Копируем данные из старой таблицы в новую
SET IDENTITY_INSERT Logo_temp ON;
INSERT INTO Logo_temp(Id, Picture)
SELECT * FROM tblLogo;
GO
SET IDENTITY_INSERT tblUser OFF;
-- Удаляем старую таблицу
DROP TABLE tblLogo;
GO
-- Переименовываем новую
EXEC sp_rename 'Logo_temp', 'tblLogo';
GO


CREATE TABLE tblComment
(
	Id INT PRIMARY KEY IDENTITY(1,1)  NOT NULL,
	UserId INT NOT NULL,
	ProductId INT,
	Comment NVARCHAR(1000),
	CommentTime DATETIME,
	CONSTRAINT FK_tblComment_UserId_tbl_tblUser_Id
		FOREIGN KEY(UserId)
			REFERENCES tblUser(Id),
	CONSTRAINT FK_tblComment_ProductId_tblProduct_Id
		FOREIGN KEY(ProductId)
			REFERENCES tblProduct(Id)
			ON DELETE CASCADE		
)



CREATE TABLE tblOrder
(	
	Id INT PRIMARY KEY IDENTITY(1,1)NOT NULL,
	UserId INT NOT NULL,
	Status BIT,
	OrderDate DATETIME,
	CONSTRAINT FK_tblOrder_UserId_tblUser_Id
		FOREIGN KEY (UserId)
			REFERENCES tblUser(Id)
)

CREATE TABLE tblOrderItem
(
	Id INT PRIMARY KEY IDENTITY(1,1) NOT NULL,	
	ProductId INT NOT NULL,
	LogoFrontId INT,
	LogoBackId INT,
	SizeId INT NOT NULL,
	OrderId INT NOT NULL,	
	Quantity INT NOT NULL,
	LogoFrontScale INT,
	LogoBackScale INT,
	IsBack BIT,
	CONSTRAINT FK_tblOrder_ProductId_tbl_Product_Id
		FOREIGN KEY (ProductId)
			REFERENCES tblProduct(Id),
	CONSTRAINT FK_tblOrder_LogoFrontId_tbl_Logo_Id
		FOREIGN KEY (LogoFrontId)
			REFERENCES tblLogo(Id),
	CONSTRAINT FK_tblOrder_LogoBackId_tbl_Logo_Id
		FOREIGN KEY (LogoBackId)
			REFERENCES tblLogo(Id),
	CONSTRAINT FK_tblOrder_SizeId_tbl_Size_Id
		FOREIGN KEY (SizeId)
			REFERENCES tblSize(Id),
	CONSTRAINT FK_tblOrderItem_OrderId_tblOrder_Id
		FOREIGN KEY (OrderId)
			REFERENCES tblOrder(Id)	
			ON DELETE CASCADE
)


