CREATE TRIGGER trigProduct_Insert
ON [tblProduct]
FOR INSERT
AS
BEGIN
	DECLARE @ID int, @Name nvarchar(30)
    SET @ID = (SELECT Id FROM INSERTED)
    Insert into tblProductSale (ProductId, SaleQuantity) 	
	VALUES(	@ID, 0);
END