USE BeStyleDB;
GO

DROP TABLE tblUserRole;
DROP TABLE tblRole;
DROP TABLE tblUser;
DROP TABLE tblProduct;
DROP TABLE tblLogo;
DROP TABLE tblCategory;
DROP TABLE tblSize;
DROP TABLE tblOrderItem;
DROP TABLE tblComment;
DROP TABLE tblOrder;
