﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BeStyle.FrontEnd.WebUI.Controllers;
using Moq;
using BeStyle.Repositories.Abstract;
using BeStyle.Entities;
using System.Collections.Generic;
using BeStyle.WebUI.Security;
using BeStyle.FrontEnd.WebUI.Models;
using System.Web.Mvc;
using System.Web;
using System.Web.Helpers;
using BeStyle.FrontEnd.WebUI.Models.AccountModels;

namespace BeStyle.FrontEnd.WebUI.Test
{
    [TestClass]
    public class SecurityControllerTest
    {
        private Mock<ISecurityManager> mockSecurity;        
        private SecurityController controller;
        [TestInitialize]
        public void Initialize_Controller()
        {            
            mockSecurity = new Mock<ISecurityManager>();
            mockSecurity.SetupGet(x => x.IsAuthenticated).Returns(true);
            mockSecurity.SetupGet(a => a.User).Returns(new UserInfo { UserId = 1, UserName = "Vitaly" });
            mockSecurity.SetupGet(x => x.IsAuthenticated).Returns(true);
            controller = new SecurityController(mockSecurity.Object);
        }
        [TestMethod]
        public void Test_Security()
        {
            var result= controller.Security() as PartialViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("_Security", result.ViewName);
            Assert.IsInstanceOfType(result.Model, typeof(SecurityModel));
        }
    }
}
