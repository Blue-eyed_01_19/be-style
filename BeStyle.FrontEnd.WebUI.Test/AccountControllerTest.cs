﻿using System;
using System.Web;
using System.Web.Mvc;
using BeStyle.Entities;
using BeStyle.FrontEnd.WebUI.Code.EmailManager;
using BeStyle.FrontEnd.WebUI.Controllers;
using BeStyle.FrontEnd.WebUI.Models.AccountModels;
using BeStyle.Repositories.Abstract;
using BeStyle.Repositories.Enums;
using BeStyle.WebUI.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json.Linq;

namespace BeStyle.FrontEnd.WebUI.Test
{
    [TestClass]
    public class AccountControllerTest
    {
        [TestMethod]
        public void Test_LoginGet()
        {
            var securityManager = new Mock<ISecurityManager>();
            securityManager.SetupGet(x => x.IsAuthenticated).Returns(false);
            var userRepository = new Mock<IUserRepository>();
            var emailManager = new Mock<IEmailManager>();
            var controller = new AccountController(securityManager.Object, userRepository.Object, emailManager.Object);
            var res = controller.Login(null);
            Assert.IsNotNull(res);
            Assert.IsInstanceOfType(res, typeof(ViewResult));
        }

        [TestMethod]
        public void Test_LoginGetRedirect()
        {
            var securityManager = new Mock<ISecurityManager>();
            securityManager.SetupGet(x => x.IsAuthenticated).Returns(true);
            var userRepository = new Mock<IUserRepository>();
            var emailManager = new Mock<IEmailManager>();
            var controller = new AccountController(securityManager.Object, userRepository.Object, emailManager.Object);
            var res = controller.Login("Shop/Shop");
            Assert.IsNotNull(res);
            Assert.IsInstanceOfType(res, typeof(RedirectToRouteResult));
        }

        [TestMethod]
        public void Test_LoginPostRedirect()
        {
            var securityManager = new Mock<ISecurityManager>();
            securityManager.Setup(
                m => m.Login(It.Is<string>(l => l == "Volodia"),
                    It.Is<string>(p => p == "Volodia"),
                    It.IsAny<bool>()))
                    .Returns(true);
            securityManager.SetupGet(x => x.IsAuthenticated).Returns(true);
            var userRepository = new Mock<IUserRepository>();
            var emailManager = new Mock<IEmailManager>();
            var controller = new AccountController(securityManager.Object, userRepository.Object, emailManager.Object);
            var res = controller.Login(new AccountModel(), null);
            Assert.IsNotNull(res);
            Assert.IsInstanceOfType(res, typeof(RedirectToRouteResult));
        }

        [TestMethod]
        public void Test_LoginPostSuccess()
        {
            var securityManager = new Mock<ISecurityManager>();
            securityManager.Setup(
                m => m.Login(It.Is<string>(l => l == "Volodia"),
                    It.Is<string>(p => p == "Volodia"),
                    It.IsAny<bool>()))
                    .Returns(true);
            securityManager.SetupGet(x => x.IsAuthenticated).Returns(false);
            var userRepository = new Mock<IUserRepository>();
            var emailManager = new Mock<IEmailManager>();
            var controller = new AccountController(securityManager.Object, userRepository.Object, emailManager.Object);
            var account = new AccountModel()
            {
                Login = new Login()
                {
                    UserName = "Volodia",
                    Password = "Volodia",
                    RememberMe = false
                }
            };
            var res = controller.Login(account, "Shop/Shop");
            Assert.IsNotNull(res);
            Assert.IsInstanceOfType(res, typeof(RedirectResult));
        }

        [TestMethod]
        public void Test_LoginPostError()
        {
            var securityManager = new Mock<ISecurityManager>();
            securityManager.Setup(
                m => m.Login(It.Is<string>(l => l == "Volodia"),
                    It.Is<string>(p => p == "Volodia"),
                    It.IsAny<bool>()))
                    .Returns(true);
            securityManager.SetupGet(x => x.IsAuthenticated).Returns(false);
            var userRepository = new Mock<IUserRepository>();
            var emailManager = new Mock<IEmailManager>();
            var controller = new AccountController(securityManager.Object, userRepository.Object, emailManager.Object);
            var account = new AccountModel()
            {
                Login = new Login()
                {
                    UserName = "FakeLogin",
                    Password = "FakePassword",
                    RememberMe = false
                }
            };
            var res = controller.Login(account, null);
            Assert.IsNotNull(res);
            Assert.IsInstanceOfType(res, typeof(ViewResult));
        }

        [TestMethod]
        public void Test_LoginPostModelStateError()
        {
            var securityManager = new Mock<ISecurityManager>();
            var userRepository = new Mock<IUserRepository>();
            var emailManager = new Mock<IEmailManager>();
            var controller = new AccountController(securityManager.Object, userRepository.Object, emailManager.Object);
            controller.ModelState.AddModelError("key", "test error");
            var account = new AccountModel();
            var res = controller.Login(account, null);
            Assert.IsNotNull(res);
            Assert.IsInstanceOfType(res, typeof(ViewResult));

        }

        [TestMethod]
        public void Test_LogOut()
        {
            var securityManager = new Mock<ISecurityManager>();
            var userRepository = new Mock<IUserRepository>();
            var emailManager = new Mock<IEmailManager>();
            var controller = new AccountController(securityManager.Object, userRepository.Object, emailManager.Object);
            var res = controller.LogOut(null);
            Assert.IsNotNull(res);
            Assert.IsInstanceOfType(res, typeof(RedirectToRouteResult));
        }

        [TestMethod]
        public void Test_LogOutReturnUrl()
        {
            var securityManager = new Mock<ISecurityManager>();
            var userRepository = new Mock<IUserRepository>();
            var emailManager = new Mock<IEmailManager>();
            var controller = new AccountController(securityManager.Object, userRepository.Object, emailManager.Object);
            var res = controller.LogOut("example");
            Assert.IsNotNull(res);
            Assert.IsInstanceOfType(res, typeof(RedirectResult));
        }

        [TestMethod]
        public void Test_RegistrationSuccess()
        {
            var securityManager = new Mock<ISecurityManager>();
            var userRepository = new Mock<IUserRepository>();
            userRepository.Setup(m => m.CreateUser(
                It.Is<UserEntity>(a => a.Login == "FreeLogin" &&
                    a.Email == "freemail@gmail.com")))
                .Returns(true);
            var emailManager = new Mock<IEmailManager>();
            var controller = new AccountController(securityManager.Object, userRepository.Object, emailManager.Object);
            var model = new AccountModel()
            {
                Registration = new Registration()
                {
                    Login = "FreeLogin",
                    Email = "freemail@gmail.com",
                    Password = "12345",
                    PasswordConfirmation = "12345"
                }
            };
            var res = controller.Registration(model, true);
            Assert.IsNotNull(res);
            Assert.IsInstanceOfType(res, typeof(JsonResult));
            var jsonRes = (JsonResult)res;
            var o = JObject.FromObject(jsonRes.Data);
            Assert.IsTrue((bool)o["Status"]);
        }

        [TestMethod]
        public void Test_RegistrationFailedPasswordsDoesntMatch()
        {
            var securityManager = new Mock<ISecurityManager>();
            var userRepository = new Mock<IUserRepository>();
            userRepository.Setup(m => m.CreateUser(
                It.Is<UserEntity>(a => a.Login == "FreeLogin" &&
                    a.Email == "freemail@gmail.com")))
                .Returns(true);
            var emailManager = new Mock<IEmailManager>();
            var controller = new AccountController(securityManager.Object, userRepository.Object, emailManager.Object);
            var model = new AccountModel()
            {
                Registration = new Registration()
                {
                    Login = "FreeLogin",
                    Email = "freemail@gmail.com",
                    Password = "12345",
                    PasswordConfirmation = "54321"
                }
            };
            var res = controller.Registration(model, true);
            Assert.IsNotNull(res);
            Assert.IsInstanceOfType(res, typeof(JsonResult));
            var jsonRes = (JsonResult)res;
            var o = JObject.FromObject(jsonRes.Data);
            Assert.IsFalse((bool)o["Status"]);
        }

        [TestMethod]
        public void Test_RegistrationFailedLoginIsBusy()
        {
            var securityManager = new Mock<ISecurityManager>();
            var userRepository = new Mock<IUserRepository>();
            userRepository.Setup(m => m.CreateUser(
                It.Is<UserEntity>(a => a.Login == "BusyLogin" &&
                    a.Email == "busymail@gmail.com")))
                .Returns(false);
            var emailManager = new Mock<IEmailManager>();
            var controller = new AccountController(securityManager.Object, userRepository.Object, emailManager.Object);
            var model = new AccountModel()
            {
                Registration = new Registration()
                {
                    Login = "BusyLogin",
                    Email = "busymail@gmail.com",
                    Password = "12345",
                    PasswordConfirmation = "12345"
                }
            };
            var res = controller.Registration(model, true);
            Assert.IsNotNull(res);
            Assert.IsInstanceOfType(res, typeof(JsonResult));
            var jsonRes = (JsonResult)res;
            var o = JObject.FromObject(jsonRes.Data);
            Assert.IsFalse((bool)o["Status"]);

        }

        [TestMethod]
        public void Test_RegistrationModelStateError()
        {
            var securityManager = new Mock<ISecurityManager>();
            var userRepository = new Mock<IUserRepository>();
            var emailManager = new Mock<IEmailManager>();
            var controller = new AccountController(securityManager.Object, userRepository.Object, emailManager.Object);
            controller.ModelState.AddModelError("key", "test error");
            var model = new AccountModel();
            var res = controller.Registration(model, true);
            Assert.IsNotNull(res);
            Assert.IsInstanceOfType(res, typeof(ViewResult));
        }

        [TestMethod]
        public void Test_RegistrationCapchaError()
        {
            var securityManager = new Mock<ISecurityManager>();
            var userRepository = new Mock<IUserRepository>();
            var emailManager = new Mock<IEmailManager>();
            var controller = new AccountController(securityManager.Object, userRepository.Object, emailManager.Object);
            var model = new AccountModel();
            var res = controller.Registration(model, false);
            Assert.IsNotNull(res);
            Assert.IsInstanceOfType(res, typeof(JsonResult));
            var jsonRes = (JsonResult)res;
            var o = JObject.FromObject(jsonRes.Data);
            Assert.IsFalse((bool)o["Status"]);
        }

        [TestMethod]
        public void Test_ConfirmRegistrationSuccess()
        {
            var securityManager = new Mock<ISecurityManager>();
            var userRepository = new Mock<IUserRepository>();
            const string login = @"Volodia";
            const string code = @"9D2B0228-4D0D-4C23-8B49-01A698857709";
            userRepository.Setup(m => m.GetUserByLogin(It.Is<string>(l => l == login)))
                .Returns(new UserEntity()
                {
                    ActivationCode = new Guid(code)
                });
            var emailManager = new Mock<IEmailManager>();

            var controller = new AccountController(securityManager.Object, userRepository.Object, emailManager.Object);
            var ret = controller.ConfirmRegistration(login, code);
            Assert.IsNotNull(ret);
            Assert.IsInstanceOfType(ret, typeof(ViewResult));
            var view = (ViewResult)ret;
            Assert.IsNotNull(view.Model);
            Assert.IsInstanceOfType(view.Model, typeof(AccountActivationModel));
        }

        [TestMethod]
        public void Test_ConfirmRegistrationFail()
        {
            var securityManager = new Mock<ISecurityManager>();
            var userRepository = new Mock<IUserRepository>();
            const string login = @"Volodia";
            const string code = @"9D2B0228-4D0D-4C23-8B49-01A698857709";
            userRepository.Setup(m => m.GetUserByLogin(It.Is<string>(l => l == login)))
                .Returns(new UserEntity()
                {
                    ActivationCode = new Guid(code)
                });
            var emailManager = new Mock<IEmailManager>();

            var controller = new AccountController(securityManager.Object, userRepository.Object, emailManager.Object);
            var ret = controller.ConfirmRegistration(login, Guid.Empty.ToString());
            Assert.IsNotNull(ret);
            Assert.IsInstanceOfType(ret, typeof(RedirectToRouteResult));
        }

        [TestMethod]
        public void Test_ProfileGetSuccess()
        {
            var securityManager = new Mock<ISecurityManager>();
            var userId = 1;
            securityManager.SetupGet(p => p.User).Returns(new UserInfo() { UserId = userId });
            var userRepository = new Mock<IUserRepository>();
            userRepository.Setup(m => m.GetUserById(It.Is<int>(id => id == userId)))
                .Returns(new UserEntity());
            var emailManager = new Mock<IEmailManager>();

            var controller = new AccountController(securityManager.Object, userRepository.Object, emailManager.Object);
            var ret = controller.UserProfile();
            Assert.IsNotNull(ret);
            Assert.IsInstanceOfType(ret, typeof(ViewResult));
            var view = (ViewResult)ret;
            Assert.IsNotNull(view.Model);
            Assert.IsInstanceOfType(view.Model, typeof(ProfileModel));
        }

        [TestMethod]
        public void Test_ProfilePostSuccess()
        {
            var securityManager = new Mock<ISecurityManager>();
            const int userId = 1;
            securityManager.SetupGet(p => p.User).Returns(new UserInfo() { UserId = userId });
            var userRepository = new Mock<IUserRepository>();
            const string login = "Success";
            const string mail = "success@gmail.com";
            const string firstName = "FSuccess";
            const string lastName = "LSuccess";
            userRepository.Setup(m => m.UpdateMainUserInfo(It.Is<UserEntity>(
                u => u.Login == login &&
                     u.Email == mail &&
                     u.FirstName == firstName &&
                     u.LastName == lastName)))
                     .Returns(UpdateUserInfo.Success);
            var emailManager = new Mock<IEmailManager>();
            var controller = new AccountController(securityManager.Object, userRepository.Object, emailManager.Object);
            var model = new ProfileModel()
            {
                Email = mail,
                FirstName = firstName,
                LastName = lastName,
                Login = login,
                UserId = userId
            };
            var ret = controller.UserProfile(model);
            Assert.IsNotNull(ret);
            Assert.IsInstanceOfType(ret, typeof(JsonResult));
            var jsonRes = (JsonResult)ret;
            var o = JObject.FromObject(jsonRes.Data);
            Assert.IsTrue((bool)o["Status"]);
        }

        [TestMethod]
        public void Test_ProfilePostLoginAndEmailIsBusy()
        {
            var securityManager = new Mock<ISecurityManager>();
            var userRepository = new Mock<IUserRepository>();
            const string login = "BusyLogin";
            const string mail = "busymail@gmail.com";
            const string firstName = "FSuccess";
            const string lastName = "LSuccess";
            const int userId = 1;
            userRepository.Setup(m => m.UpdateMainUserInfo(It.Is<UserEntity>(
                u => u.Login == login &&
                     u.Email == mail &&
                     u.FirstName == firstName &&
                     u.LastName == lastName)))
                     .Returns(UpdateUserInfo.LoginAndEmailIsBusy);
            var emailManager = new Mock<IEmailManager>();
            var controller = new AccountController(securityManager.Object, userRepository.Object, emailManager.Object);
            var model = new ProfileModel()
            {
                Email = mail,
                FirstName = firstName,
                LastName = lastName,
                Login = login,
                UserId = userId
            };
            var ret = controller.UserProfile(model); 
            Assert.IsNotNull(ret);
            Assert.IsInstanceOfType(ret, typeof(JsonResult));
            var jsonRes = (JsonResult)ret;
            var o = JObject.FromObject(jsonRes.Data);
            Assert.IsFalse((bool)o["Status"]);
        }

        [TestMethod]
        public void Test_ProfilePostLoginIsBusy()
        {
            var securityManager = new Mock<ISecurityManager>();
            var userRepository = new Mock<IUserRepository>();
            const string login = "BusyLogin";
            const string mail = "success@gmail.com";
            const string firstName = "FSuccess";
            const string lastName = "LSuccess";
            const int userId = 1;
            userRepository.Setup(m => m.UpdateMainUserInfo(It.Is<UserEntity>(
                u => u.Login == login &&
                     u.Email == mail &&
                     u.FirstName == firstName &&
                     u.LastName == lastName)))
                     .Returns(UpdateUserInfo.LoginIsBusy);
            var emailManager = new Mock<IEmailManager>();
            var controller = new AccountController(securityManager.Object, userRepository.Object, emailManager.Object);
            var model = new ProfileModel()
            {
                Email = mail,
                FirstName = firstName,
                LastName = lastName,
                Login = login,
                UserId = userId
            };
            var ret = controller.UserProfile(model); 
            Assert.IsNotNull(ret);
            Assert.IsInstanceOfType(ret, typeof(JsonResult));
            var jsonRes = (JsonResult)ret;
            var o = JObject.FromObject(jsonRes.Data);
            Assert.IsFalse((bool)o["Status"]);
        }

        [TestMethod]
        public void Test_ProfilePostEmailIsBusy()
        {
            var securityManager = new Mock<ISecurityManager>();
            var userRepository = new Mock<IUserRepository>();
            const string login = "Success";
            const string mail = "busymail@gmail.com";
            const string firstName = "FSuccess";
            const string lastName = "LSuccess";
            const int userId = 1;
            userRepository.Setup(m => m.UpdateMainUserInfo(It.Is<UserEntity>(
                u => u.Login == login &&
                     u.Email == mail &&
                     u.FirstName == firstName &&
                     u.LastName == lastName)))
                     .Returns(UpdateUserInfo.EmailIsBusy);
            var emailManager = new Mock<IEmailManager>();
            var controller = new AccountController(securityManager.Object, userRepository.Object, emailManager.Object);
            var model = new ProfileModel()
            {
                Email = mail,
                FirstName = firstName,
                LastName = lastName,
                Login = login,
                UserId = userId
            };
            var ret = controller.UserProfile(model); 
            Assert.IsNotNull(ret);
            Assert.IsInstanceOfType(ret, typeof(JsonResult));
            var jsonRes = (JsonResult)ret;
            var o = JObject.FromObject(jsonRes.Data);
            Assert.IsFalse((bool)o["Status"]);
        }

        [TestMethod]
        public void Test_ProfilePostInvalidModelState()
        {
            var securityManager = new Mock<ISecurityManager>();
            var userRepository = new Mock<IUserRepository>();
            const string login = "Success";
            const string mail = "busymail@gmail.com";
            const string firstName = "FSuccess";
            const string lastName = "LSuccess";
            const int userId = 1;
            userRepository.Setup(m => m.UpdateMainUserInfo(It.Is<UserEntity>(
                u => u.Login == login &&
                     u.Email == mail &&
                     u.FirstName == firstName &&
                     u.LastName == lastName)))
                     .Returns(UpdateUserInfo.EmailIsBusy);
            var emailManager = new Mock<IEmailManager>();
            var controller = new AccountController(securityManager.Object, userRepository.Object, emailManager.Object);
            var model = new ProfileModel();
            controller.ModelState.AddModelError("key", "test error");
            var ret = controller.UserProfile(model); 
            Assert.IsNotNull(ret);
            Assert.IsInstanceOfType(ret, typeof(ViewResult));
            var view = (ViewResult)ret;
            Assert.IsNotNull(view.Model);
            Assert.IsInstanceOfType(view.Model, typeof(ProfileModel));
        }

        [TestMethod]
        public void Test_ProfilePostDefaultStatement()
        {
            var securityManager = new Mock<ISecurityManager>();
            var userRepository = new Mock<IUserRepository>();
            const string login = "Success";
            const string mail = "mail@gmail.com";
            const string firstName = "FSuccess";
            const string lastName = "LSuccess";
            const int userId = 1;
            userRepository.Setup(m => m.UpdateMainUserInfo(It.Is<UserEntity>(
                u => u.Login == login &&
                     u.Email == mail &&
                     u.FirstName == firstName &&
                     u.LastName == lastName)))
                     .Returns((UpdateUserInfo)int.MaxValue);
            var emailManager = new Mock<IEmailManager>();
            var controller = new AccountController(securityManager.Object, userRepository.Object, emailManager.Object);
            var model = new ProfileModel()
            {
                Email = mail,
                FirstName = firstName,
                LastName = lastName,
                Login = login,
                UserId = userId
            };
            var ret = controller.UserProfile(model); 
            Assert.IsNotNull(ret);
            Assert.IsInstanceOfType(ret, typeof(ViewResult));
            var view = (ViewResult)ret;
            Assert.IsNotNull(view.Model);
            Assert.IsInstanceOfType(view.Model, typeof(ProfileModel));
        }

        [TestMethod]
        public void Test_ChangePasswordPartialGetSuccess()
        {
            const int userId = 1;
            var securityManager = new Mock<ISecurityManager>();
            securityManager.SetupGet(p => p.User).Returns(new UserInfo() { UserId = userId });
            var userRepository = new Mock<IUserRepository>();
            var emailManager = new Mock<IEmailManager>();

            var controller = new AccountController(securityManager.Object, userRepository.Object, emailManager.Object);
            var ret = controller.ChangePasswordPartial();
            Assert.IsNotNull(ret);
            Assert.IsInstanceOfType(ret, typeof(PartialViewResult));
            var partialView = (PartialViewResult) ret;
            Assert.IsNotNull(partialView.Model);
            Assert.IsInstanceOfType(partialView.Model, typeof(ChangePasswordModel));
        }

        [TestMethod]
        public void Test_ChangePasswordPartialPostSuccess()
        {
            const int userId = 1;
            const string password = "12345";
            var securityManager = new Mock<ISecurityManager>();
            securityManager.SetupGet(p => p.User).Returns(new UserInfo() { UserId = userId });
            securityManager.Setup(m => m.CryptPassword(It.Is<string>(p => p == password))).Returns(password);
            var userRepository = new Mock<IUserRepository>();
            userRepository.Setup(m => m.GetUserById(It.Is<int>(id => id == userId)))
                .Returns(new UserEntity()
                {
                    Password = password
                });

            var emailManager = new Mock<IEmailManager>();

            var controller = new AccountController(securityManager.Object, userRepository.Object, emailManager.Object);
            var newPassword = "54321";
            var model = new ChangePasswordModel()
            {
                UserId = userId,
                Password = password,
                PasswordConfirmation = newPassword,
                NewPassword = newPassword
            };
            var ret = controller.ChangePasswordPartial(model);
            Assert.IsNotNull(ret);
            Assert.IsInstanceOfType(ret, typeof(JsonResult));
            var jsonRes = (JsonResult)ret;
            var o = JObject.FromObject(jsonRes.Data);
            Assert.IsTrue((bool)o["Status"]);
        }

        [TestMethod]
        public void Test_ChangePasswordPartialPostConfirmError()
        {
            const int userId = 1;
            const string password = "12345";
            var securityManager = new Mock<ISecurityManager>();
            securityManager.SetupGet(p => p.User).Returns(new UserInfo() { UserId = userId });
            securityManager.Setup(m => m.CryptPassword(It.Is<string>(p => p == password))).Returns(password);
            var userRepository = new Mock<IUserRepository>();
            userRepository.Setup(m => m.GetUserById(It.Is<int>(id => id == userId)))
                .Returns(new UserEntity()
                {
                    Password = password
                });

            var emailManager = new Mock<IEmailManager>();

            var controller = new AccountController(securityManager.Object, userRepository.Object, emailManager.Object);
            const string newPassword = "54321";
            const string badPassword = "55544";
            var model = new ChangePasswordModel()
            {
                UserId = userId,
                Password = password,
                PasswordConfirmation = newPassword,
                NewPassword = badPassword
            };
            var ret = controller.ChangePasswordPartial(model);
            Assert.IsNotNull(ret);
            Assert.IsInstanceOfType(ret, typeof(JsonResult));
            var jsonRes = (JsonResult)ret;
            var o = JObject.FromObject(jsonRes.Data);
            Assert.IsFalse((bool)o["Status"]);
        }

        [TestMethod]
        public void Test_ChangePasswordPartialPostInvalidPassword()
        {
            const int userId = 1;
            const string password = "12345";
            var securityManager = new Mock<ISecurityManager>();
            securityManager.SetupGet(p => p.User).Returns(new UserInfo() { UserId = userId });
            securityManager.Setup(m => m.CryptPassword(It.Is<string>(p => p == password))).Returns(password);
            var userRepository = new Mock<IUserRepository>();
            userRepository.Setup(m => m.GetUserById(It.Is<int>(id => id == userId)))
                .Returns(new UserEntity()
                {
                    Password = password
                });

            var emailManager = new Mock<IEmailManager>();

            var controller = new AccountController(securityManager.Object, userRepository.Object, emailManager.Object);
            const string newPassword = "54321";
            const string badPassword = "55544";
            var model = new ChangePasswordModel()
            {
                UserId = userId,
                Password = badPassword,
                PasswordConfirmation = newPassword,
                NewPassword = newPassword
            };
            var ret = controller.ChangePasswordPartial(model);
            Assert.IsNotNull(ret);
            Assert.IsInstanceOfType(ret, typeof(JsonResult));
            var jsonRes = (JsonResult)ret;
            var o = JObject.FromObject(jsonRes.Data);
            Assert.IsFalse((bool)o["Status"]);
        }

        [TestMethod]
        public void Test_RestorePasswordGet()
        {
            var securityManager = new Mock<ISecurityManager>();
            var userRepository = new Mock<IUserRepository>();
            var emailManager = new Mock<IEmailManager>();

            var controller = new AccountController(securityManager.Object, userRepository.Object, emailManager.Object);
            var ret = controller.RestorePassword();
            Assert.IsNotNull(ret);
            Assert.IsInstanceOfType(ret, typeof(PartialViewResult));
        }

        [TestMethod]
        public void Test_RestorePasswordPostSuccess()
        {
            const string email = "success@gmail.com";
            const int userId = 1;
            const string password = "12345";
            var securityManager = new Mock<ISecurityManager>();
            securityManager.SetupGet(p => p.User).Returns(new UserInfo() { UserId = userId });
            securityManager.Setup(m => m.CryptPassword(It.Is<string>(p => p == password))).Returns(password);
            var userRepository = new Mock<IUserRepository>();
            userRepository.Setup(m => m.GetUserByEmail(It.Is<string>(mail => mail == email)))
                .Returns(new UserEntity()
                {
                    Id = userId
                });
            var emailManager = new Mock<IEmailManager>();

            var controller = new AccountController(securityManager.Object, userRepository.Object, emailManager.Object);
            var model = new RestorePasswordModel()
            {
                Email = email
            };
            var ret = controller.RestorePassword(model);
            Assert.IsNotNull(ret);
            Assert.IsInstanceOfType(ret, typeof(JsonResult));
            var jsonRes = (JsonResult)ret;
            var o = JObject.FromObject(jsonRes.Data);
            Assert.IsTrue((bool)o["Status"]);
        }

        [TestMethod]
        public void Test_RestorePasswordPostIncorrectEmail()
        {
            const string email = "failmail@gmail.com";
            const int userId = 1;
            const string password = "12345";
            var securityManager = new Mock<ISecurityManager>();
            securityManager.SetupGet(p => p.User).Returns(new UserInfo() { UserId = userId });
            securityManager.Setup(m => m.CryptPassword(It.Is<string>(p => p == password))).Returns(password);
            var userRepository = new Mock<IUserRepository>();
            userRepository.Setup(m => m.GetUserByEmail(It.Is<string>(mail => mail == email)))
                .Returns<UserEntity>(null);
            var emailManager = new Mock<IEmailManager>();

            var controller = new AccountController(securityManager.Object, userRepository.Object, emailManager.Object);
            var model = new RestorePasswordModel()
            {
                Email = email
            };
            var ret = controller.RestorePassword(model);
            Assert.IsNotNull(ret);
            Assert.IsInstanceOfType(ret, typeof(JsonResult));
            var jsonRes = (JsonResult)ret;
            var o = JObject.FromObject(jsonRes.Data);
            Assert.IsFalse((bool)o["Status"]);
        }

        [TestMethod]
        public void Test_RestorePasswordPostModelStateError()
        {
            const string email = "success@gmail.com";
            const int userId = 1;
            const string password = "12345";
            var securityManager = new Mock<ISecurityManager>();
            securityManager.SetupGet(p => p.User).Returns(new UserInfo() { UserId = userId });
            securityManager.Setup(m => m.CryptPassword(It.Is<string>(p => p == password))).Returns(password);
            var userRepository = new Mock<IUserRepository>();
            userRepository.Setup(m => m.GetUserByEmail(It.Is<string>(mail => mail == email)))
                .Returns(new UserEntity()
                {
                    Id = userId
                });
            var emailManager = new Mock<IEmailManager>();

            var controller = new AccountController(securityManager.Object, userRepository.Object, emailManager.Object);
            controller.ModelState.AddModelError("key", "test error");
            var model = new RestorePasswordModel()
            {
                Email = email
            };
            var ret = controller.RestorePassword(model);
            Assert.IsNotNull(ret);
            Assert.IsInstanceOfType(ret, typeof(JsonResult));
            var jsonRes = (JsonResult)ret;
            var o = JObject.FromObject(jsonRes.Data);
            Assert.IsFalse((bool)o["Status"]);
        }

    }
}
