﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BeStyle.FrontEnd.WebUI.Controllers;
using Moq;
using BeStyle.Repositories.Abstract;
using BeStyle.Entities;
using System.Collections.Generic;
using BeStyle.WebUI.Security;
using BeStyle.FrontEnd.WebUI.Models;
using System.Web.Mvc;
using System.Web;
using System.Web.Helpers;

namespace BeStyle.FrontEnd.WebUI.Test
{
    [TestClass]
    public class ShopControllerTest
    {
        private Mock<IProductRepository> mockProducts;
        private Mock<ISizeRepository> mockSizes;
        private Mock<ICategoryRepository> mockCategories;
        private Mock<ICommentRepository> mockComments;
        private Mock<ISecurityManager> mockSecurity;
        private ShopController controller;
           
        [TestInitialize]
        public void InitializeController()
        {
            mockProducts = new Mock<IProductRepository>();
            mockProducts.Setup(a => a.GetProductsAll()).Returns(new List<ProductEntity> { new ProductEntity { Id = 1, CategoryId = 1, Price = 100 } });
            mockProducts.Setup(a => a.GetProductById(It.IsAny<int>())).Returns(new ProductEntity { Id = 1, CategoryId=1, Price=50 });
            mockProducts.Setup(a => a.GetProductsByCategoryId(It.IsAny<int>())).Returns(new List<ProductEntity> { new ProductEntity { Id = 1, CategoryId = 1, Price = 100 } });
            mockProducts.Setup(a => a.GetProductsByPriceAndCategories(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>())).Returns(new List<ProductEntity> { new ProductEntity { Id = 1, CategoryId = 1, Price = 50 } });
            mockProducts.Setup(a => a.GetProductsByPrice(It.IsAny<int>(), It.IsAny<int>())).Returns(new List<ProductEntity> { new ProductEntity { Id = 1, CategoryId = 1, Price = 10 } });
            mockSizes = new Mock<ISizeRepository>();
            mockSizes.Setup(a => a.GetSizes()).Returns(new List<SizeEntity>());
            mockCategories = new Mock<ICategoryRepository>();
            mockCategories.Setup(a => a.GetAllCategory()).Returns(new List<CategoryEntity>());
            mockComments = new Mock<ICommentRepository>();
            mockComments.Setup(a => a.GetComments()).Returns(new List<CommentEntity>());
            mockSecurity = new Mock<ISecurityManager>();
            mockSecurity.SetupGet(a => a.User).Returns(new UserInfo { UserId = 1, UserName = "Vitaly" });            
            controller = new ShopController(mockProducts.Object, mockSizes.Object,
                                            mockCategories.Object, mockSecurity.Object,
                                            mockComments.Object);         
        }
        
        [TestMethod]
        public void Test_Shop()
        {           
            var result=controller.Shop() as ViewResult;
            Assert.IsNotNull(result.Model);           
        }
        [TestMethod]
        public void Test_ShopByCategory()
        {            
            var result = controller.ShopByCategory(1) as ViewResult;
            Assert.AreEqual("Shop", result.ViewName);
            Assert.IsNotNull(result.Model);            
        }
        [TestMethod]
        public void ShopByPrice()
        {           
            var result = controller.ShopByPrice(1, 100, 1) as PartialViewResult;
            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void Test_HeaderBottom()
        {            
            var result = controller.HeaderBottom() as PartialViewResult;
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Model);
            Assert.IsInstanceOfType(result.Model, typeof(List<CategoryEntity>));
        }
        [TestMethod]
        public void Test_ProductPartial()
        {           
            var contextMock = new Mock<ControllerContext>();
            var mockHttpContext = new Mock<HttpContextBase>();
            var session = new Mock<HttpSessionStateBase>();
            mockHttpContext.Setup(ctx => ctx.Session).Returns(session.Object);
            contextMock.Setup(ctx => ctx.HttpContext).Returns(mockHttpContext.Object);
            controller.ControllerContext = contextMock.Object;                  
            var result = controller.ProductPartial(1) as PartialViewResult;
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result.Model, typeof(ProductModel));
        }
        [TestMethod]
        public void Test_AddComment()
        {                       
            JsonResult result = controller.AddComment(1, "Good") as JsonResult;
            Assert.IsNotNull(result.Data);
        }
    }
}
