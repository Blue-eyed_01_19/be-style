﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using BeStyle.Repositories.Abstract;
using BeStyle.WebUI.Security;
using BeStyle.FrontEnd.WebUI.Controllers;
using System.Web.Mvc;
using System.Web;
using BeStyle.FrontEnd.WebUI.Models;
using BeStyle.Entities;
using System.Collections.Generic;
using System.Web.SessionState;
using BeStyle.FrontEnd.WebUI.Code;

namespace BeStyle.FrontEnd.WebUI.Test
{
    [TestClass]
    public class CartControllerTest
    {
        private Mock<IProductRepository> mockProducts;
        private Mock<ISizeRepository> mockSizes;
        private Mock<ILogoRepository> mockLogos;
        private Mock<ISecurityManager> mockSecurity;
        private Mock<IOrderRepository> mockOrders;     
        private Mock<ControllerContext> mockControllerContext;
        private Mock<HttpSessionStateBase> mockSession;     
        private Mock<Cart> Cart;

        private CartController controller;
        [TestInitialize]
        public void InitializeController()
        {            
            Cart = new Mock<Cart>();
            mockProducts = new Mock<IProductRepository>();
            mockProducts.Setup(a => a.GetProductById(1)).Returns(new ProductEntity { Id = 1, Name = "Adidas" });
            mockSizes = new Mock<ISizeRepository>();
            mockSizes.Setup(a => a.GetSizeValueById(1)).Returns(new SizeEntity { Id = 1, Size = "S" });
            mockOrders = new Mock<IOrderRepository>();
            mockLogos = new Mock<ILogoRepository>();
            mockLogos.Setup(a => a.GetLogoById(1)).Returns(new LogoEntity { Id = 1, IsUser = true });
            mockSecurity = new Mock<ISecurityManager>();
            mockSecurity.SetupGet(a => a.User).Returns(new UserInfo { UserId = 1, UserName = "Vitaly" });            
            mockControllerContext = new Mock<ControllerContext>();
            mockSession = new Mock<HttpSessionStateBase>();
            mockSession.SetupGet(s => s[SessionKeys.CART_SESSION_KEY]).Returns(Cart.Object);
            mockControllerContext.Setup(p => p.HttpContext.Session).Returns(mockSession.Object);
            controller = new CartController(mockProducts.Object, mockSizes.Object, mockOrders.Object, mockSecurity.Object, mockLogos.Object);
            controller.ControllerContext = mockControllerContext.Object;                      
        }

        [TestMethod]
        public void Test_CartSummary()
        {
            var result = controller.CartSummary() as ViewResult;
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Model);
            Assert.IsInstanceOfType(result.Model, typeof(CartModel));
        }
        [TestMethod]
        public void Test_AddToCart_UserLogoNotNull()
        {
            var result = controller.AddToCart(1, 1, 0, 0, 0, 0, "", "") as JsonResult;
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Data);
        }
        [TestMethod]
        public void Test_AddToCart_UserLogoIsNull()
        {
            var result = controller.AddToCart(1, 1, 0, 0, 0, 0, null, null) as JsonResult;
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Data);
        }
        [TestMethod]
        public void Test_AddToCart_FromShop()
        {
            var result = controller.AddToCart(1, 1, 1, 1, 1, 1, null, null) as JsonResult;
            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void Test_ClearCart()
        {
            var result = controller.ClearCart() as RedirectToRouteResult;
            Assert.IsNotNull(result);
            Assert.IsTrue(result.RouteValues.ContainsKey("action"));
            Assert.IsTrue(result.RouteValues.ContainsKey("controller"));
            Assert.AreEqual("CartSummary", result.RouteValues["action"].ToString());
            Assert.AreEqual("Cart", result.RouteValues["controller"].ToString());
        }
        [TestMethod]
        public void Test_RemoveFromCart()
        {
            var result = controller.RemoveFromCart(1, 1) as JsonResult;
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Data);
        }
        [TestMethod]
        public void Test_SubmitOrder_IsAuth()
        {
            var item1 = new CartItem
            {
                Product = new ProductEntity { Id = 1, Price = 5 },
                LogoFront = null,
                Size = new SizeEntity { Id = 1 }
            };
            var item2 = new CartItem
            {
                Product = new ProductEntity { Id = 1, Price = 5 },
                LogoFront = new LogoEntity { Id = 1 },
                LogoBack = new LogoEntity { Id = 1 },
                Size = new SizeEntity { Id = 1 },
                LogoBackScale = 100,
                LogoFrontScale = 50
            };
            Cart.SetupGet(a => a.Items).Returns(new List<CartItem> { item1, item2 });
            mockSecurity.SetupGet(a => a.IsAuthenticated).Returns(true);        
            var result = controller.SubmitOrder() as JsonResult;
            Assert.IsNotNull(result.Data);
        }
        [TestMethod]
        public void Test_SubmitOrder_EmptyCart()
        {
            mockSecurity.SetupGet(a => a.IsAuthenticated).Returns(true);
            mockSession.SetupGet(s => s[SessionKeys.CART_SESSION_KEY]).Returns(null);
            var result = controller.SubmitOrder() as JsonResult;
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Data);
        }
        [TestMethod]
        public void Test_SubmitOrder_NotAuth()
        {
            mockSecurity.SetupGet(a => a.IsAuthenticated).Returns(false);
            var result = controller.SubmitOrder() as JsonResult;
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Data);
        }
    }
}
