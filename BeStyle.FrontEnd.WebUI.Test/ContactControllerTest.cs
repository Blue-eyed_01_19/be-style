﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using BeStyle.WebUI.Security;
using BeStyle.FrontEnd.WebUI.Controllers;
using System.Web.Mvc;
using BeStyle.FrontEnd.WebUI.Code.EmailManager;

namespace BeStyle.FrontEnd.WebUI.Test
{
    [TestClass]
    public class ContactControllerTest
    {
        private Mock<ISecurityManager> mockSecurity;
        private Mock<IEmailManager> mockEmail;
        private ContactController controller;
        [TestInitialize]
        public void InitializeController()
        {
            mockSecurity = new Mock<ISecurityManager>();
            mockSecurity.SetupGet(a => a.IsAuthenticated).Returns(true);
            mockSecurity.SetupGet(a => a.User).Returns(new UserInfo { UserId = 1, UserName = "Vitaly" });     
            mockEmail = new Mock<IEmailManager>();
            controller = new ContactController(mockSecurity.Object, mockEmail.Object);
        }
        [TestMethod]
        public void Test_Contact()
        {
            var result = controller.Contact() as ViewResult;
            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void Test_SendEmailtoUs_IsAuthentificated()
        {
            var result = controller.SendEmailtoUs("name", "text") as JsonResult;
            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void Test_SendEmailtoUs_NoneAuthentificated()
        {
            mockSecurity.SetupGet(a => a.IsAuthenticated).Returns(false);
            var result = controller.SendEmailtoUs("name", "text") as JsonResult;
            Assert.IsNotNull(result);
        }
    }
}
